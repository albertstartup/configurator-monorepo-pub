# Product Option Configurator

## - Organization of Document
 - Description of the configurator.
 - Usage of the configurator dashboard.
 - Description of the monorepo.
 - Description of the frontend.
 - Description of the backend.
 - Misc (Keywords).

## - Description of the configurator

A configurator invalidates product options based on shopper selection. A selection may invalidate some other option. For example, say a shopper wants a Tesla with spinning rims. Elon Musk decides that only Roadsters can have spinning rims. A configurator should invalidate spinning rims if a shopper selects anything but the Roadster. I have talked to people on Twitter about possibly applying this to real estate. For example, say a mom wants to buy a house. Her most important criteria is that the schools be rated 4 stars or higher. Houses in neighborhoods with such schools cost $5 million and higher. A configurator can immediately remove the option of choosing houses less than $5 million. If the mom then decides that she can't afford that, she can then reconsider her criteria and select 3 stars schools. Automated invalidation reduces much back and forth between a buyer and a seller.

## - Usage of the configurator dashboard
 A seller wants to sell custom products. He creates options that a product can have. Each option has many option values. Each option value can be specified to invalidate other option values. The seller then creates a product and specifies which option values to include and specifies how the option value affects the base price. The backend then creates the product in Shopify and stores the option value data on Shopify's backend. The Shopify theme code then loads that data into the configurator.

## - Description of the monorepo

Root - Lerna, Yarn Workspaces, Monorepo created by merging two previously separate repos.

* .vscode - Chrome Debugging, Single TypeScript File Debugging, Auto Attach For Node+TS debugging.

* packages/common - Planning to share Typegoose types between front and backend. Mongoose also does client-side validation, so I want to do that instead of Jsonschema.

## - Description of the frontend

Frontend - Next JS, Strongly Typed Redux, Observables/Epics, Jest, TDD, React Hooks, Ant Design, Ramda, Typescript generics
  
  * Pages + Redux

    * Connecting the page components to redux is done per page. Alternatively, the connection could be made on the parent container of all pages.

    * mapDispatchToProps maps both sync action creators and async action creators. This was not trivial.
      * Every page has its props typed.

    * pages/_app.jsx - Router + Redux Spinner Logic

    * pages/manageProducts.tsx - Usage of lenses in autocomplete code.

    * pages/editRule.tsx - Generic function to swap mongo object. See Misc.

  * Specs - Jest Tests For Configurator, Experiment with JSVerify Property-Based Testing (From Haskell Community)

  * src/react

    * configurator - Configurator initially used graph theory with an in-browser graph database. After reading papers and talking to co-workers, I decided that having a blacklist first invalidation system would be simpler and effective. Map/reduce is used extensively.
  
      * configurator/shopify-configurator.tsx - A preview of what the customer will see is available in the dashboard, this same preview can be compiled and embedded into the Shopify theme file. The babel and webpack configuration is not trivial.

  * src/lib/Layout.tsx - Using relative positioning was critical for a consistent layout. Spinner and Notification code is isolated to this file.

  * src/lib/lib.tsx - Connects to the backend. Jsonschema used for data validation. Typescript types where compiled to Jsonschemas.

  * src/react/selectorComponent - Extensive use of generics and Ramda. Selector takes a list of Mongo objects and allows selecting multiple or single and toggling.

  * src/redux - Strongly Typed, Redux Devtools, Redux-Observable, Typesafe-Actions
    * Usage of Redux - I tried to limit the use of Redux. Other projects I have done used too much Redux, making it hard to modularize. Instead, I embrace prop drilling.
    * Epics - Epics are used for data fetching. When a data fetch is requested the spinner is activated.

  * src/old - HOC and Render Prop based Layout Component, Configurator with reason for invalidation.

## - Description of the backend

Backend - Typegoose, Mongoose, TS-Node debugging, Mongo duplication prevention, Boom error handling, dotenv

  * TS-Node + VS Code debugging. Nontrivial.

  * src/lib.ts - Types, Typegoose, Async route wrapper

  * src/index.ts - CORS, Proper error handling, Separation of routes.

  * src/mongoSetup.ts - Prevents duplication with indexes, rather than custom code.

## - Misc (Keywords)

### Stongly Typed Redux and Redux-Observables.
### Typegoose
### Babel for shopify rendering

### Swap Mongo Objects
``` typescript
const swapMongoObject = <T extends { _id: string }>(newMongoObject: T, list: T[]): T[] => {
  const without = R.filter<T>(R.where({
    "_id": (valueToCheck: string) => {
      valueToCheck !== newMongoObject._id
    }
  }), list)
  return R.union(without, [newMongoObject])
}
```

### Make variable global
``` ts
export const makeVariablesGlobal = (objectWithVariables: {}) => {
    if (process.browser) {
        const currentPrivateVariables = (window as any).p
        const newPrivateVariables = { ...currentPrivateVariables, ...objectWithVariables };
        (window as any).p = newPrivateVariables
    }
}
```

### Practical understanding of CSS
- Spinner in Layout. 

- Only Layout has information on spinner state.

### Map reduce

Usage in configurator

### Spinner Logic Placement

### Sharing common code between backend and frontend

### Generating jsonschema from typescript types for api
See createProduct route.

### Server Response Validation with Jsonschema

### Proper Separation of Routes

### Proper Error Handling

### Lenses and deep understanding of hooks
See manageProducts autocomplete code.

### Proper usage of Hooks (Hooks don't execute on every render)

### Limited and scoped used of Redux

### Embracing Prop Drilling over Redux, while still not being over verbose

This is an anti-pattern if you have already declared the component props' type.
``` typescript
type Props = {}

const {var1, var2} = props
```