import { AutoComplete, Card, Menu, Button, Input, Form } from 'antd'
import Layout from '../src/react/lib/Layout'
import { FunctionComponent, useState, useEffect } from 'react';
import store from '../src/redux/store';
import * as R from 'ramda'
import Link from 'next/link';
import { ProductOption, OptionValue } from '..';
import { extractProductOptionWithId, setProductOptionsInStore, makeVariablesGlobal, setOptionValuesInStore, createOptionValue } from '../src/react/lib/lib';

import * as actions from '../src/redux/actions'
import { Provider, connect } from 'react-redux';
import Router, { withRouter, WithRouterProps } from 'next/router'
import { bindActionCreators, Dispatch, AnyAction } from 'redux';
import { RootState } from '../src/redux/reducers';
import { CustomDivider } from '../src/react/lib/componentLib';

type Props = {
    productOptions: ProductOption[],
    setProductOptions: (productOptions: ProductOption[]) => void,
    initialEditingOption?: ProductOption,
    optionValues: OptionValue[],
    setOptionValues: (optionValue: OptionValue[]) => void
}

makeVariablesGlobal({ R })

const Page: FunctionComponent<Props & WithRouterProps> = ({ initialEditingOption = null, ...props }) => {
    const [value, setValue] = useState('')
    const [editingProductOption, setEditingProductOption] = useState(initialEditingOption)
    const [isCreatingOptionValue, setIsCreatingOptionValue] = useState(false)

    useEffect(() => {
        // @ts-ignore
        props.fetchProductOptions.request()
        // @ts-ignore
        props.fetchOptionValues.request()
        // setProductOptionsInStore(props.productOptions, props.setProductOptions)
        // setOptionValuesInStore(props.optionValues, props.setOptionValues)
    }, [false])

    let optionId: string | false | undefined | null

    Router.events.on('routeChangeComplete', () => {
        console.log('route changed')
        const urlParams = new URLSearchParams(window.location.search)

        optionId = urlParams.get('id')

        if (optionId) {
            const option = extractProductOptionWithId(optionId, props.productOptions)
            option && setEditingProductOption(option)
        } else {
            setEditingProductOption(initialEditingOption)
        }
    })

    const router = props.router
    const query = router && router.query
    optionId = query && query.id && typeof (query.id) === 'string' && query.id

    useEffect(() => {
        if (optionId) {
            const option = extractProductOptionWithId(optionId, props.productOptions)
            option && setEditingProductOption(option)
        }
    }, [props.productOptions])

    let headerText = 'Choose An Option To Edit'
    if (optionId && editingProductOption && !R.isEmpty(editingProductOption)) {
        headerText = `Edit Option: ${editingProductOption.name}`
    }

    const filterOptionValuesByOwnerOptionId = (ownerOptionId: string,
        optionValues: OptionValue[]): OptionValue[] => {
        return R.filter(
            R.where({ ownerOptionId: R.equals(ownerOptionId) }),
            optionValues
        )
    }

    const onSubmit = async () => {
        if (editingProductOption !== null) {
            const id = editingProductOption._id

            const response = await createOptionValue(value, id)
            props.setOptionValues(R.concat(props.optionValues, [response]))
        }
    }

    return <Layout headerText={headerText}>
        <div>
            {editingProductOption &&
                <div>
                    <CustomDivider text='Choose An Option Value To Edit:' />
                    {props.optionValues &&
                        R.map((optionValue) => {
                            return <Link href={`/editRule?optionValueId=${optionValue._id}`}><Button>{optionValue.name}</Button></Link>
                        }, filterOptionValuesByOwnerOptionId(
                            editingProductOption._id,
                            props.optionValues
                        ))
                    }
                    <CustomDivider text='Or Try These Other Actions:' />
                    {isCreatingOptionValue &&
                        <Form layout='inline' onSubmit={(e) => { e.preventDefault(); onSubmit() }}>
                            <Form.Item>
                                <Input autoFocus value={value} onChange={(e) => setValue(e.target.value)} style={{ marginTop: '1em' }}
                                    placeholder='Option Value Name' />
                            </Form.Item>
                            <Form.Item>
                                <Button htmlType='submit' type='primary'
                                    style={{ marginTop: '1em' }}>Create New Option Value</Button>
                            </Form.Item>
                        </Form>
                    }
                    {!isCreatingOptionValue &&
                        <Button onClick={() => setIsCreatingOptionValue(true)} type='primary'
                            style={{ marginTop: '1em' }}>Create New Option Value</Button>
                    }
                </div>
            }
            {!editingProductOption &&
                R.map((option) => {
                    return <Link href={`/editOption?id=${option._id}`}>
                        <Button type='dashed'>
                            {option.name}
                        </Button>
                    </Link>
                }, props.productOptions)
            }
        </div>
    </Layout>
}

// Redux Code

const mapStateToProps = (state: RootState) => {
    return {...state}
}

const { async_actions, ...sync_actions } = actions
const mapDispatchToProps = (dispatch: Dispatch<AnyAction>) => {
    return {
        ...bindActionCreators({ ...sync_actions }, dispatch),
        ...R.mapObjIndexed((async_action) => {
            return bindActionCreators(async_action, dispatch)
        }, actions.async_actions)
    }
}

const Connected = connect(mapStateToProps, mapDispatchToProps)(withRouter(Page))

export default () => <Provider store={store}>
    <Connected/>
</Provider>