import React from 'react'
import App, { Container } from 'next/app'
import Router from 'next/router'
import store from '../src/redux/store'
import { activateSpinner, deactivateSpinner } from '../src/redux/actions'

Router.events.on('routeChangeStart', url => {
    console.log(`Loading: ${url}`)
    store.dispatch(activateSpinner())
})
Router.events.on('routeChangeComplete', () => store.dispatch(deactivateSpinner()))
Router.events.on('routeChangeError', () => store.dispatch(deactivateSpinner()))

class MyApp extends App {
    static async getInitialProps({ Component, ctx }) {
        let pageProps = {}

        if (Component.getInitialProps) {
            pageProps = await Component.getInitialProps(ctx)
        }

        return { pageProps }
    }

    render() {
        const { Component, pageProps } = this.props

        return (
            <Container>                
                <Component {...pageProps} />
            </Container>
        )
    }
}

export default MyApp