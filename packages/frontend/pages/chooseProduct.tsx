import Layout from '../src/react/lib/Layout';
import ProductDescriptionFilter from '../src/react/selectorComponents/ProductDescriptionFilter'
import { FunctionComponent, useEffect, useState } from 'react'
import ProductSelector from '../src/react/selectorComponents/ProductSelector';
import { ProductType, ProductDescriptionType, ProductCategory } from '..';
import { AppActions } from '../src/redux/reducers';
import { setProductsAndProductDescriptions, setProductCategoriesInStore, setProductsInStore, makeVariablesGlobal, fetchProductCategories } from '../src/react/lib/lib';
import * as R from 'ramda';

import * as actions from '../src/redux/actions'
import { Provider, connect } from 'react-redux';
import { bindActionCreators, Dispatch, AnyAction } from 'redux';
import { RootState } from '../src/redux/reducers';
import store from '../src/redux/store'

import Router, { withRouter, WithRouterProps, SingletonRouter } from 'next/router'

type Props = {
    products: ProductType[],
    productDescriptions: ProductDescriptionType[],
    productCategories: ProductCategory[],
    setProductCategories: (x: ProductCategory[]) => any
    setProducts: (products: ProductType[]) => any,
    setProductDescriptions: (productDescriptions: ProductDescriptionType[]) => any,
}

const Page: FunctionComponent<Props> = (props) => {
    makeVariablesGlobal({props})
    const { products, productDescriptions,
        productCategories, setProductCategories,
        setProducts, setProductDescriptions } = props

    useEffect(() => {
        //setProductsAndProductDescriptions(products, setProducts, setProductDescriptions)                
        // @ts-ignore
        props.fetchProducts.request()
        //setProductsInStore(props.products, props.setProducts)
    }, [false])

    // Only rerun this if products changes.
    useEffect(() => {
        //setProductCategoriesInStore(productCategories, setProductCategories)
        // @ts-ignore
        props.fetchProductCategories.request()
    }, [products])

    const [selectedProductType, setSelectedProductType] = useState('')
    const [selectedPrintType, setSelectedPrintType] = useState('')
    const [selectedProductCategory, setSelectedProductCategory] = useState<ProductCategory | undefined>(undefined)

    console.log({ selectedProductType, selectedPrintType, selectedProductCategory });

    return <Layout headerText="Choose A Product To Edit:">

        <ProductDescriptionFilter productCategories={productCategories} onSelectProductCategory={selected => setSelectedProductCategory(selected[0])}
            onSelectProductType={setSelectedProductType}
            onSelectPrintType={setSelectedPrintType} productTypeDividerText='Filter By Product Type:'
            printTypeDividerText='Filter By Print Type:' productCategoryDivideText='Filter By Product Category' />

        <ProductSelector products={products} productDescriptions={productDescriptions}
            filterObj={{ productCategoryId: selectedProductCategory && selectedProductCategory._id || '', printType: selectedPrintType, productType: selectedProductType }} dividerText='Choose A Product To Edit' />
    </Layout>
}

// Redux Code

const mapStateToProps = (state: RootState) => {
    return { ...state }
}

const { async_actions, ...sync_actions } = actions
const mapDispatchToProps = (dispatch: Dispatch<AnyAction>) => {
    return {
        ...bindActionCreators({...sync_actions}, dispatch),
        ...R.mapObjIndexed((async_action) => {
            return bindActionCreators(async_action, dispatch)
        }, actions.async_actions)
    }
}

const Connected = connect(mapStateToProps, mapDispatchToProps)(withRouter(Page))

export default () => <Provider store={store}>
    <Connected />
</Provider>