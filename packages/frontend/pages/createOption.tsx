import { AutoComplete, Card, Menu, Input, Button, Form } from 'antd'
import Layout from '../src/react/lib/Layout'
import { FunctionComponent, useState} from 'react';
import store from '../src/redux/store';
import axios from 'axios'
import { AppActions } from '../src/redux/reducers';
import { ProductOption } from '..';
import * as R from 'ramda'
import { createProductOption } from '../src/react/lib/lib';
import { toast } from 'react-toastify'

import * as actions from '../src/redux/actions'
import { Provider, connect } from 'react-redux';
import { bindActionCreators, Dispatch, AnyAction } from 'redux';
import { RootState } from '../src/redux/reducers';

type Props = {
    productOptions: ProductOption[],
    setProductOptions: (productOptions: ProductOption[]) => void
}

const CreateOptionPage: FunctionComponent<Props> = (props) => {
    const [value, setValue] = useState('')

    const onSubmit = async () => {
        const response = await createProductOption(value)
        props.setProductOptions(R.concat(props.productOptions, [response]))
        toast.success('Created Option!')
    }

    return <Layout headerText='Create A New Product Option'>
        <div>Name The New Product Option:</div>
        <Form onSubmit={(e) => {e.preventDefault(); onSubmit()}}>
            <Form.Item>
                <Input value={value} onChange={(e) => setValue(e.target.value)} style={{ marginTop: '1em' }}
                placeholder='What is the new option called?' />
            </Form.Item>
            <Form.Item>
                <Button htmlType='submit' type='primary' style={{ marginTop: '1em' }}>Create</Button>
            </Form.Item>
        </Form>
    </Layout>
}

// Redux Code

const mapStateToProps = (state: RootState) => {
    return {
        productOptions: state.productOptions
    }
}

const { async_actions, ...sync_actions } = actions
const mapDispatchToProps = (dispatch: Dispatch<AnyAction>) => {
    return {
        ...bindActionCreators({ ...sync_actions }, dispatch),
        ...R.mapObjIndexed((async_action) => {
            return bindActionCreators(async_action, dispatch)
        }, actions.async_actions)
    }
}

const ConnectedCreateOptionPage = connect(mapStateToProps, mapDispatchToProps)(CreateOptionPage)

const ReduxCreateProductPage = () => <Provider store={store}>
    <ConnectedCreateOptionPage />
</Provider>

export default ReduxCreateProductPage