import CategoryCreator from '../src/react/CategoryCreator'
import { useEffect, FunctionComponent, useState } from 'react';
import store from '../src/redux/store';
import CreateProduct from '../src/react/Product/CreateProduct'
import Layout from '../src/react/lib/Layout';
import { Row, Col, Input, AutoComplete, Button, InputNumber } from 'antd'
import Selector from '../src/react/selectorComponents/Selector';
import { OptionValue, ProductType, ProductCategory, ProductOption, ProductOptions } from '..';
import * as R from 'ramda'
import { toast } from 'react-toastify'
import { Collapse } from 'antd'

import * as actions from '../src/redux/actions'
import { Provider, connect } from 'react-redux';
import Router, { withRouter, WithRouterProps, SingletonRouter } from 'next/router'
import { bindActionCreators, Dispatch, AnyAction } from 'redux';
import { RootState } from '../src/redux/reducers';

import { filterOptionValuesByOwnerOptionIds, findCategoryWithId, createProduct, findProductWithId, updateProduct, makeVariablesGlobal, getOptionValueWithIds } from '../src/react/lib/lib';
import { OptionProps, LabeledValue } from 'antd/lib/select';
import Configurator from '../src/react/configurator/ConfiguratorUI';
import ReactDOMServer from 'react-dom/server'

type Props = {
    products: ProductType[],
    setProducts: (x: ProductType[]) => any
    productCategories: ProductCategory[],
    setProductCategories: (x: ProductCategory[]) => any,
    productOptions: ProductOption[],
    setProductOptions: (productOptions: ProductOption[]) => void,
    optionValues: OptionValue[],
    setOptionValues: (optionValue: OptionValue[]) => void
}

const Page: FunctionComponent<Props & WithRouterProps> = (props) => {
    // TODO: Create hook such that you can do:
    // const [products, setProducts] = useProducts()
    useEffect(() => {
        // setProductCategoriesInStore(props.productCategories, props.setProductCategories)
        // setProductsInStore(props.products, props.setProducts)
        // setProductOptionsInStore(props.productOptions, props.setProductOptions),
        // setOptionValuesInStore(props.optionValues, props.setOptionValues)        
        // @ts-ignore
        props.fetchProducts.request()
        // @ts-ignore
        props.fetchProductOptions.request()
        // @ts-ignore
        props.fetchOptionValues.request()
        // @ts-ignore
        props.fetchProductCategories.request()
    }, [false])

    const [productName, setProductName] = useState('')
    const [selectedCategoryId, setSelectedCategoryId] = useState('')
    const [selectedPrintType, setSelectedPrintType] = useState('')
    const [selectedProductType, setSelectedProductType] = useState('')
    const [selectedOptionValues, setSelectedOptionValues] = useState<ProductOptions>({})

    const postProductionsOptions = R.filter<ProductOption>((prod) => {
        return R.includes(prod.name, ['Bindery'])
    }, props.productOptions)

    const selectedCategory = findCategoryWithId(selectedCategoryId,
        props.productCategories)

    const changeSelectedOptionValues = (optionId: string, selectedIds: string[], currentSelectedOptionValues: ProductOptions) => {
        const newSelectedOptionValues = { ...currentSelectedOptionValues }
        newSelectedOptionValues[optionId] = selectedIds
        console.log(newSelectedOptionValues)
        return newSelectedOptionValues
    }

    const appendToSelectedOptionValues = (optionId: string, newId: string, currentSelectedOptionValues: ProductOptions) => {
        const newSelectedOptionValues = { ...currentSelectedOptionValues }
        if (!newSelectedOptionValues[optionId]) { newSelectedOptionValues[optionId] = [] }
        const newValues = newSelectedOptionValues[optionId].concat([newId])
        newSelectedOptionValues[optionId] = newValues
        return newSelectedOptionValues
    }

    const removeFromSelectedOptionValues = (optionId: string, idToRemove: string, currentSelectedOptionValues: ProductOptions) => {
        const newSelectedOptionValues = { ...currentSelectedOptionValues }
        const newValues = R.without([idToRemove], newSelectedOptionValues[optionId])
        newSelectedOptionValues[optionId] = newValues
        return newSelectedOptionValues
    }

    const router = props.router
    const query = router && router.query
    const productId = query && query.id && typeof (query.id) === 'string' && query.id
    const editingProduct = productId && findProductWithId(productId, props.products)

    useEffect(() => {
        if (editingProduct) {
            setProductName(editingProduct.name)
            setSelectedPrintType(editingProduct.printType)
            setSelectedProductType(editingProduct.productType)
            setSelectedCategoryId(editingProduct.productCategoryId)
            setSelectedOptionValues(editingProduct.initialOptions)
        }
    }, [editingProduct])

    makeVariablesGlobal({ selectedCategoryId, selectedPrintType })

    const onClickSave = async () => {
        if (editingProduct) {
            // @ts-ignore
            props.activateSpinner()
            await updateProduct(editingProduct._id, {
                _id: editingProduct._id,
                name: productName,
                printType: selectedPrintType,
                productType: selectedProductType,
                productCategoryId: selectedCategoryId,
                initialOptions: selectedOptionValues
            })
            // @ts-ignore
            props.deactivateSpinner()
            toast.success('Updated Product!')
        } else {
            // @ts-ignore
            props.activateSpinner()
            await createProduct(productName,
                selectedPrintType, selectedProductType,
                selectedCategoryId, selectedOptionValues)
            // @ts-ignore
            props.deactivateSpinner()
            toast.success('Created Product!')
        }
    }

    const [autocompVals, setAutocompVals] = useState<Record<string, string>>({})

    // <Configurator initialOptions={selectedOptionValues}
    // optionValues={props.optionValues} productOptions={props.productOptions} />

    return <Layout headerText='Create A New Product'>
        <Row style={{ marginBottom: '2em' }} gutter={24}>
            <Col span={12}>
                <div>
                    <div style={{ textAlign: 'center', marginBottom: '4em' }}>
                        Job Info
                    </div>
                    <div style={{ marginBottom: '2em' }}>
                        <div style={{
                            marginBottom: '1em'
                        }}>
                            Product Name
                        </div>
                        <Input value={productName}
                            onChange={e => setProductName(e.target.value)}
                            style={{ width: '65%' }}
                            placeholder='Enter Name' />
                    </div>
                    <TextInput value={selectedPrintType} onSelect={selectedKey => setSelectedPrintType(selectedKey)}
                        dataSource={['Digital', 'Offset']}
                        heading='Print Type:'
                        placeholder='Enter Print Type' />
                    <TextInput value={selectedProductType} onSelect={selectedKey => setSelectedProductType(selectedKey)}
                        dataSource={['Book', 'Flat', 'Large Format']}
                        heading='Product Type:'
                        placeholder='Enter Product Type' />
                    {props.productCategories && !R.isEmpty(props.productCategories) &&
                        <TextInput value={selectedCategory ? {
                            key: selectedCategory._id, label: selectedCategory.name
                        } : selectedCategoryId}
                            onSelect={selectedKey => setSelectedCategoryId(selectedKey)}
                            placeholder='Enter Product Category'
                            heading="Product Category">
                            {R.map((category) => {
                                return <Option key={category._id}>{category.name}</Option>
                            }, props.productCategories)}
                        </TextInput>
                    }
                </div>
            </Col>
            <Col span={12}>
                <Row style={{ marginBottom: '2em' }} gutter={16}>
                    <div style={{
                        marginBottom: '4em',
                        textAlign: "center"
                    }}>
                        Paper Info
                        </div>
                    <div>
                        {!R.isEmpty(props.productOptions) && R.map((option) => {
                            return <div key={option._id}>
                                <div style={{ marginBottom: '1em' }}>Select {option.name}:</div>
                                <AutoComplete
                                    value={autocompVals[option._id]}
                                    onSelect={(value) => {
                                        setSelectedOptionValues(
                                            appendToSelectedOptionValues(option._id, value.toString(), selectedOptionValues)
                                        )
                                        setAutocompVals(R.set(R.lensProp(option._id), '', autocompVals))
                                    }}
                                    onSearch={(value) => {
                                        setAutocompVals(R.set(R.lensProp(option._id), value, autocompVals))
                                    }}
                                    filterOption={(inputValue, option) => {
                                        if (!option ||
                                            !option.props || !option.props.children ||
                                            !(typeof (option.props.children) === 'string')) throw Error
                                        return option.props.children.toUpperCase().indexOf(
                                            inputValue.toUpperCase()) !== -1
                                    }}>
                                    {R.map((optionValue) => {
                                        return <AutoComplete.Option key={optionValue._id}>{optionValue.name}</AutoComplete.Option>
                                    }, filterOptionValuesByOwnerOptionIds([option._id],
                                        props.optionValues))}
                                </AutoComplete>
                                <div>
                                    {R.map((optionValue) => {
                                        return <div style={{ marginTop: '0.5rem' }}>
                                            <Button
                                                icon="delete"
                                                onClick={() => {
                                                    setSelectedOptionValues(removeFromSelectedOptionValues(option._id, optionValue._id, selectedOptionValues))
                                                }}>{optionValue.name}</Button>
                                        </div>
                                    }, getOptionValueWithIds(selectedOptionValues[option._id] || [], props.optionValues))}
                                </div>
                                {/* <Selector defaultSelectedItems={getOptionValueWithIds(selectedOptionValues[option._id] || [], props.optionValues)}
                                onSelect={selected => {
                                    setSelectedOptionValues(
                                        changeSelectedOptionValues(option._id, R.pluck('_id', selected), selectedOptionValues)
                                    )
                                }} multiple
                                    style={{ marginBottom: '1em' }}
                                    items={
                                        filterOptionValuesByOwnerOptionIds([option._id],
                                            props.optionValues)} /> */}
                            </div>
                        }, R.without(postProductionsOptions, props.productOptions))}
                    </div>
                    <div style={{ marginTop: '4em' }}>
                        <div style={{ marginBottom: '2em', textAlign: "center" }}>
                            Post Productions:
                        </div>
                        {!R.isEmpty(props.productOptions) && R.map((option) => {
                            return <div key={option._id}>
                                <div style={{ marginBottom: '1em' }}>Select {option.name}:</div>
                                <AutoComplete
                                    value={autocompVals[option._id]}
                                    onSelect={(value) => {
                                        setSelectedOptionValues(
                                            appendToSelectedOptionValues(option._id, value.toString(), selectedOptionValues)
                                        )
                                        setAutocompVals(R.set(R.lensProp(option._id), '', autocompVals))
                                    }}
                                    onSearch={(value) => {
                                        setAutocompVals(R.set(R.lensProp(option._id), value, autocompVals))
                                    }}
                                    filterOption={(inputValue, option) => {
                                        if (!option ||
                                            !option.props || !option.props.children ||
                                            !(typeof (option.props.children) === 'string')) throw Error
                                        return option.props.children.toUpperCase().indexOf(
                                            inputValue.toUpperCase()) !== -1
                                    }}>
                                    {R.map((optionValue) => {
                                        return <AutoComplete.Option key={optionValue._id}>{optionValue.name}</AutoComplete.Option>
                                    }, filterOptionValuesByOwnerOptionIds([option._id],
                                        props.optionValues))}
                                </AutoComplete>
                                <div>
                                    {R.map((optionValue) => {
                                        return <div style={{ marginTop: '0.5rem' }}>
                                            <Button
                                                icon="delete"
                                                onClick={() => {
                                                    setSelectedOptionValues(removeFromSelectedOptionValues(option._id, optionValue._id, selectedOptionValues))
                                                }}>{optionValue.name}</Button>
                                        </div>
                                    }, getOptionValueWithIds(selectedOptionValues[option._id] || [], props.optionValues))}
                                </div>
                                {/* <Selector defaultSelectedItems={getOptionValueWithIds(selectedOptionValues[option._id] || [], props.optionValues)} onSelect={selected => {
                                    setSelectedOptionValues(
                                        changeSelectedOptionValues(option._id, R.pluck('_id', selected), selectedOptionValues)
                                    )
                                }} multiple items={
                                    filterOptionValuesByOwnerOptionIds([option._id],
                                        props.optionValues)} /> */}
                            </div>
                        }, postProductionsOptions)}
                    </div>
                </Row>
            </Col>
        </Row>
        {/* <Row>
            <Col span={12}>
                <div style={{ textAlign: 'center', marginBottom: '4em' }}>
                    Pricing
                </div>
                <Collapse defaultActiveKey={['1']}>
                    {R.map((option) => {
                        return <Collapse.Panel header={option.name} key={option._id}>
                            <div style={{ marginTop: '1em' }}>                                
                                {R.map((optionValue) => {
                                    return <div style={{ marginTop: '0.5em' }}>
                                        <div>
                                            {optionValue.name}
                                        </div>
                                        <InputNumber />
                                    </div>
                                }, getOptionValueWithIds(selectedOptionValues[option._id] || [], props.optionValues))}
                            </div>
                        </Collapse.Panel>
                    }, props.productOptions)}
                </Collapse>
            </Col>
        </Row> */}
        <div style={{ textAlign: 'center' }}>
            <Button onClick={() => onClickSave()} type='primary'>Save Product</Button>
        </div>
        <div style={{ marginTop: '4em' }}>
            <Configurator initialOptions={selectedOptionValues}
                optionValues={props.optionValues} productOptions={props.productOptions} />
        </div>
    </Layout>
}

type TextInputProps = {
    heading: string,
    placeholder: string,
    dataSource?: any[],
    children?: Array<React.ReactElement<OptionProps>>,
    onSelect: (selectedKey: string) => void,
    value: string | LabeledValue
}

const Option = AutoComplete.Option

const TextInput: FunctionComponent<TextInputProps> = (props) => {

    const labelValue = typeof (props.value) === 'object' &&
        typeof (props.value.label) === 'string' ?
        props.value.label : undefined

    const [searchValue, setSearchValue] = useState(labelValue)

    return <div style={{ marginBottom: '2em' }}>
        <div style={{
            marginBottom: '1em'
        }}>
            {props.heading}:
        </div>
        {props.children &&
            <AutoComplete value={searchValue}
                filterOption={(inputValue, option) => {
                    if (!option ||
                        !option.props || !option.props.children ||
                        !(typeof (option.props.children) === 'string')) throw Error
                    return option.props.children.toUpperCase().indexOf(
                        inputValue.toUpperCase()) !== -1
                }}
                onSearch={(value) => {
                    setSearchValue(value)
                }}
                onSelect={(selectedKey) => {
                    props.onSelect(selectedKey.toString())
                    setSearchValue(selectedKey.toString())
                }}
                style={{ width: '65%' }}
                placeholder={props.placeholder}>
                {props.children}
            </AutoComplete>
        }
        {!props.children &&
            <AutoComplete value={props.value}
                onSelect={(selectedKey) => props.onSelect(selectedKey.toString())}
                style={{ width: '65%' }}
                dataSource={props.dataSource} placeholder={props.placeholder} />
        }
    </div>
}

// Redux Code

const mapStateToProps = (state: RootState) => {
    return { ...state }
}

const { async_actions, ...sync_actions } = actions
const mapDispatchToProps = (dispatch: Dispatch<AnyAction>) => {
    return {
        ...bindActionCreators({ ...sync_actions }, dispatch),
        ...R.mapObjIndexed((async_action) => {
            return bindActionCreators(async_action, dispatch)
        }, actions.async_actions)
    }
}

const Connected = connect(mapStateToProps, mapDispatchToProps)(withRouter(Page))

export default () => <Provider store={store}>
    <Connected />
</Provider>