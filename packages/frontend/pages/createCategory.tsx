import { FunctionComponent, useState, useEffect, FormEvent } from 'react';
import Layout from '../src/react/lib/Layout';
import store from '../src/redux/store';
import ProductDescriptionFilter from '../src/react/selectorComponents/ProductDescriptionFilter';
import { createProductCategory } from '../src/react/lib/lib';
import 'antd/dist/antd.css'
import { Input, Button, Form } from 'antd';
import { AppActions } from '../src/redux/reducers';
import { ProductCategory } from '..';
import { ClipLoader } from 'react-spinners'
import * as R from 'ramda'
import { CustomDivider } from '../src/react/lib/componentLib';

import * as actions from '../src/redux/actions'
import { Provider, connect } from 'react-redux';
import Router, { withRouter, WithRouterProps, SingletonRouter } from 'next/router'
import { bindActionCreators, Dispatch, AnyAction } from 'redux';
import { RootState } from '../src/redux/reducers';

import { toast } from 'react-toastify';

type Props = {
    productCategories: ProductCategory[],
    setProductCategories: (x: ProductCategory[]) => any
}

const Page: FunctionComponent<Props> = (props) => {
    const [value, setValue] = useState('')

    useEffect(() => {
        //setProductCategoriesInStore(productCategories, setProductCategories)
    })

    const onClickCreate = async (e: FormEvent) => {
        e.preventDefault()
        // @ts-ignore
        props.activateSpinner()
        const createdProductCategory = await createProductCategory(value)
        // @ts-ignore
        props.deactivateSpinner()
        props.setProductCategories(R.concat(props.productCategories, [createdProductCategory]))
        toast.success('Created Category!')
    }

    return <Layout headerText='Create New Product Category:'>
        {/* <ProductDescriptionFilter productTypeDividerText='Choose A Product Type' printTypeDividerText='Choose a Print Type' /> */}

        <Form onSubmit={(e) => onClickCreate(e)}>
            {/* <CustomDivider text='Name the New Product Category: ' /> */}
            <div style={{ paddingBottom: '1em' }}>Name the New Product Category:</div>
            <Form.Item>
                <Input onChange={(e) => setValue(e.target.value)} value={value} style={{ width: '20em' }} size="large" placeholder="Name of The New Product Category" />
            </Form.Item>
            <Form.Item>
                <Button style={{ marginLeft: '1em' }}
                htmlType="submit"
                type="primary"
                size='large'>Create Category</Button>
            </Form.Item>
        </Form>
    </Layout>
}

// Redux Code

const mapStateToProps = (state: RootState) => {
    return { ...state }
}

const { async_actions, ...sync_actions } = actions
const mapDispatchToProps = (dispatch: Dispatch<AnyAction>) => {
    return {
        ...bindActionCreators({ ...sync_actions }, dispatch),
        ...R.mapObjIndexed((async_action) => {
            return bindActionCreators(async_action, dispatch)
        }, actions.async_actions)
    }
}

const Connected = connect(mapStateToProps, mapDispatchToProps)(withRouter(Page))

export default () => <Provider store={store}>
    <Connected />
</Provider>