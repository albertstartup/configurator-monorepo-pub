import { AutoComplete, Card, Menu, Button, Input, Form } from 'antd'
import Layout from '../src/react/lib/Layout'
import { FunctionComponent, useState, useEffect } from 'react';
import store from '../src/redux/store';
import * as R from 'ramda'
import Link from 'next/link';
import { ProductOption, OptionValue } from '..';

import * as actions from '../src/redux/actions'
import { Provider, connect } from 'react-redux';
import Router, { withRouter, WithRouterProps, SingletonRouter } from 'next/router'
import { bindActionCreators, Dispatch, AnyAction } from 'redux';
import { RootState } from '../src/redux/reducers';
import { CustomDivider } from '../src/react/lib/componentLib';

type Props = {
    productOptions: ProductOption[],
    setProductOptions: (x: ProductOption[]) => void,    
    optionValues: OptionValue[],
    setOptionValues: (x: OptionValue[]) => void
}

const Page: FunctionComponent<Props & WithRouterProps> = (props) => {    

    return <Layout headerText={'Edit Option Value Blue:'}>
        <div>
            <CustomDivider text='Choose An Action:'/>
            <Button type='primary'>Create Rule</Button>
        </div>
    </Layout>
}

// Redux Code

const mapStateToProps = (state: RootState) => {
    return {...state}
}

const { async_actions, ...sync_actions } = actions
const mapDispatchToProps = (dispatch: Dispatch<AnyAction>) => {    
    return {
        ...bindActionCreators({...sync_actions}, dispatch),
        ...R.mapObjIndexed((async_action) => {
            return bindActionCreators(async_action, dispatch)
        }, actions.async_actions)
    }
}

const Connected = connect(mapStateToProps, mapDispatchToProps)(withRouter(Page))

export default () => <Provider store={store}>
    <Connected/>
</Provider>