import { AutoComplete, Card, Menu, Button, Input, Form } from 'antd'
import Layout from '../src/react/lib/Layout'
import { FunctionComponent, useState, useEffect } from 'react';
import store from '../src/redux/store';
import * as R from 'ramda'
import Link from 'next/link';
import { ProductOption, OptionValue, Invalidates } from '..';
import { setProductOptionsInStore, makeVariablesGlobal, setOptionValuesInStore, filterOptionValuesByOwnerOptionIds, updateOptionValue, isIn } from '../src/react/lib/lib';

import * as actions from '../src/redux/actions'
import { Provider, connect } from 'react-redux';
import Router, { withRouter, WithRouterProps, SingletonRouter } from 'next/router'
import { bindActionCreators, Dispatch, AnyAction } from 'redux';
import { RootState } from '../src/redux/reducers';

import Selector from '../src/react/selectorComponents/Selector';
import { CustomDivider } from '../src/react/lib/componentLib'
import { toast } from 'react-toastify';

type Props = {
    productOptions: ProductOption[],
    setProductOptions: (x: ProductOption[]) => void,
    optionValues: OptionValue[],
    setOptionValues: (x: OptionValue[]) => void
}

const createInvalidatesFromSelectedOptionValues = (optionValues: OptionValue[]):
    Invalidates => {
    const grouped = R.groupBy((optionValue) => {
        return optionValue.ownerOptionId
    }, optionValues)

    return R.mapObjIndexed((optionValues) => {
        return R.pluck('_id', optionValues)
    }, grouped)
}

/**
 * Given a new mongo object, find the old mongo object by id, swap it out with new object 
 * and return new list
 * @param newMongoObject 
 * @param list 
 */
const swapMongoObject = <T extends { _id: string }>(newMongoObject: T,
    list: T[]): T[] => {
    const without = R.filter<T>(R.where({
        "_id": (valueToCheck: string) => valueToCheck !== newMongoObject._id
    }), list)
    return R.union(without, [newMongoObject])
}

const getInvalidatedOptionValues = (invalidates: Invalidates,
    allOptionValues: OptionValue[]): OptionValue[] => {
    const values = R.values(invalidates)
    const empty: string[] = []
    const all = empty.concat(...values)
    const filtered = R.filter<OptionValue>((optionValue) => {
        return R.includes(optionValue._id, all)
    }, allOptionValues)
    return filtered
}

const Page: FunctionComponent<Props & WithRouterProps> = (props) => {
    const [selectedOptions, setSelectedOptions] = useState<ProductOption[]>([])
    const [selectedOptionValues, setSelectedOptionValues] = useState<OptionValue[]>([])

    useEffect(() => {
        //setProductOptionsInStore(props.productOptions, props.setProductOptions)
        //setOptionValuesInStore(props.optionValues, props.setOptionValues)
        // @ts-ignore
        props.fetchProductOptions.request()
        // @ts-ignore
        props.fetchOptionValues.request()
    }, [false])

    const router = props.router
    const query = router && router.query
    const optionValueId = query && query.optionValueId &&
        typeof (query.optionValueId) === 'string' && query.optionValueId

    const editingOptionValue = R.find<OptionValue>(R.where({ _id: R.equals(optionValueId) }), props.optionValues)

    const optionValuesWithoutSelfOptionValue = editingOptionValue ?
        R.without([editingOptionValue], props.optionValues) : props.optionValues

    const filteredOptionValues =
        filterOptionValuesByOwnerOptionIds(R.pluck('_id', selectedOptions), optionValuesWithoutSelfOptionValue)

    const optionValuesToShow = R.isEmpty(filteredOptionValues) ?
        optionValuesWithoutSelfOptionValue : filteredOptionValues

    const onClickSave = async () => {
        if (!optionValueId) return
        const newInvalidates = createInvalidatesFromSelectedOptionValues(selectedOptionValues)
        const updatedOptionValue = await updateOptionValue(optionValueId, { invalidates: newInvalidates })
        props.setOptionValues(swapMongoObject(updatedOptionValue, props.optionValues))
        toast.success('Saved Rule!')
    }

    const defaultSelected = editingOptionValue ?
        getInvalidatedOptionValues(editingOptionValue.invalidates, props.optionValues) : []

    const headerText = `Edit rule for ${editingOptionValue ? editingOptionValue.name : ''}: `

    //R.without([editingOptionValue], )

    return <Layout headerText={headerText}>
        <div>
            <div style={{ 'marginBottom': '6em' }}>
                <CustomDivider text='Filter By Option:' />
                <Selector onSelect={(newSelectedItems) => {
                    setSelectedOptions(newSelectedItems)
                }} items={props.productOptions} />
            </div>
            <div style={{ 'marginBottom': '6em' }}>
                <CustomDivider text='Choose Which Option Values To Invalidate:' />
                <Selector<OptionValue> defaultSelectedItems={defaultSelected} multiple
                    onSelect={(items) => setSelectedOptionValues(items)} items={optionValuesToShow} />
            </div>
            <div style={{ textAlign: 'center' }}>
                <Button type='primary' onClick={() => onClickSave()}>Save Rule</Button>
            </div>
        </div>
    </Layout>
}

// Redux Code

const mapStateToProps = (state: RootState) => {
    return { ...state }
}

const { async_actions, ...sync_actions } = actions
const mapDispatchToProps = (dispatch: Dispatch<AnyAction>) => {
    return {
        ...bindActionCreators({ ...sync_actions }, dispatch),
        ...R.mapObjIndexed((async_action) => {
            return bindActionCreators(async_action, dispatch)
        }, actions.async_actions)
    }
}

const Connected = connect(mapStateToProps, mapDispatchToProps)(withRouter(Page))

export default () => <Provider store={store}>
    <Connected />
</Provider>