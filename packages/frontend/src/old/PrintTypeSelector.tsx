import * as R from 'ramda'
import { Button, Radio } from 'antd'
import 'antd/dist/antd.css'
import { FunctionComponent } from 'react';

type PrintTypeSelectorProps = {
    
    printTypes: string[]
}

const PrintTypeSelector: FunctionComponent<PrintTypeSelectorProps> = (props) => {
    const {printTypes} = props

    return <div>
        <Radio.Group>
            {
                R.map((printType) => {
                    return <Radio.Button value={printType} style={{ marginLeft: '1em' }} key={printType}>{printType}</Radio.Button>
                }, printTypes)
            }
        </Radio.Group>
    </div>
}

export default PrintTypeSelector