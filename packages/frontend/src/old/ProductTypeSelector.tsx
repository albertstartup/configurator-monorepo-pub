import * as R from 'ramda'
import { Button, Radio } from 'antd'
import 'antd/dist/antd.css'
import { FunctionComponent } from 'react';

type ProductTypeSelectorProps = {
    productTypes: string[]
}

const ProductTypeSelector: FunctionComponent<ProductTypeSelectorProps> = (props) => {
    const {productTypes} = props

    return <div>
        <Radio.Group>
            {
                R.map((productType) => {
                    return <Radio.Button value={productType} style={{ marginLeft: '1em' }}
                        key={productType}>{productType}</Radio.Button>
                }, productTypes)
            }
        </Radio.Group>
    </div>
}

export default ProductTypeSelector