import { Divider } from 'antd'

export const CustomDivider = ({ text }: { text: string }) => {
    return <Divider style={{ width: '100%' }}>
        {text}
    </Divider>
}