import axios from 'axios'
import { ProductType, ProductDescriptionType, ProductCategory, ProductOption, OptionValue, Product_NoId_NoShopifyId, Product_NoShopifyId } from '../../..'
import * as R from 'ramda'
import { validate } from 'jsonschema'

declare let API_URL: string

declare let process: { browser: boolean }

// TODO: Make this a package.
/**
 * This uses ES2015 shorthand proprety names to make variables global.
 * @param objectWithVariables An object of variables to make global. Use shorthand proprety notation ({variableName}).
 */
export const makeVariablesGlobal = (objectWithVariables: {}) => {
    if (process.browser) {
        const currentPrivateVariables = (window as any).p
        const newPrivateVariables = { ...currentPrivateVariables, ...objectWithVariables };
        (window as any).p = newPrivateVariables
    }
}

export const getProducts = async () => {
    try {
        return (await axios.get(`${API_URL}/api/products`)).data
    } catch (error) {
        throw error
    }
}

// enum PrintType = {

// }

export const createProduct = async (name: string, printType: string,
    productType: string, productCategoryId: string, initialOptions: object) => {
    try {
        const response = await axios.post(
            `${API_URL}/api/createProduct`, {
                product: {
                    name,
                    printType,
                    productType,
                    productCategoryId,
                    initialOptions
                }
            })
        const created = response.data
        return created
    } catch (error) {
        throw error
    }
}

export const fetchProducts = async (): Promise<ProductType[]> => {
    try {
        const response = await axios.get(`${API_URL}/api/getProducts`)
        validate(response.data, {
            "type": "array",
            "items": { "type": "object" }
        }, { throwError: true })
        return response.data
    } catch (error) {
        throw error
    }
}

export const setProductsInStore = async (currentItems: ProductType[],
    setProductOptions: (x: ProductType[]) => void) => {
    if (R.isEmpty(currentItems)) {
        console.log('product options is empty')

        const fetched = await fetchProducts()

        setProductOptions(fetched)
    }
}

export const getProductDescription = async (productId: string) => {
    try {
        return (await axios.get(`${API_URL}/api/getProductDescription/${productId}`)).data
    } catch (error) {
        throw error
    }
}

export const getProductDescriptionsAndId = async (productIds: string[]) => {
    return await Promise.all(R.map(async (productId: string) => {
        const productDescription = await getProductDescription(productId)
        return { productId, ...productDescription }
    }, productIds))
}

export const createProductCategory = async (categoryName: string) => {
    try {
        return (await axios.post(`${API_URL}/api/createProductCategory/${categoryName}`)).data
    } catch (error) {
        throw error
    }
}

export const createOptionValue = async (name: string, ownerOptionId: string): Promise<OptionValue> => {
    try {
        const response = await axios.post(
            `${API_URL}/api/createOptionValue`, {
                name: name,
                ownerOptionId: ownerOptionId
            })
        const created = response.data
        return created
    } catch (error) {
        throw error
    }
}

export const isIn = <T extends {}>(listToCheckIn: T[]) => {
    return (itemToCheckFor: T) => {
        return R.includes(itemToCheckFor, listToCheckIn)
    }
}

export const getOptionValueWithIds = (ids: string[], optionValues: OptionValue[]): OptionValue[] => {
    return R.filter<OptionValue>((option) => {
        return R.includes(option._id, ids)
    }, optionValues)
}

export const getOptionNameWithId = (optionId: string, options: ProductOption[]): string | undefined => {
    const option = R.find<ProductOption>(R.where({ "_id": R.equals(optionId) }), options)
    console.log('No option with id: ', optionId);
    return option ? option.name : undefined
}


export const filterOptionValuesByOwnerOptionIds = (ownerOptionIds: string[],
    optionValues: OptionValue[]): OptionValue[] => {

    return R.filter(
        R.where({ ownerOptionId: isIn(ownerOptionIds) }),
        optionValues
    )
}

export const createProductOption = async (optionName: string) => {
    try {
        return (await axios.post(`${API_URL}/api/createProductOption/${optionName}`)).data
    } catch (error) {
        throw error
    }
}

export const fetchProductOptions = async (): Promise<ProductOption[]> => {
    try {
        const response = await axios.get(`${API_URL}/api/getProductOptions`)
        validate(response.data, {
            "type": "array",
            "items": { "type": "object" }
        }, { throwError: true })
        return response.data
    } catch (error) {
        throw error
    }
}

export const fetchOptionValues = async (): Promise<OptionValue[]> => {
    try {
        const response = await axios.get(`${API_URL}/api/getOptionValues`)
        validate(response.data, {
            "type": "array",
            "items": { "type": "object" }
        }, { throwError: true })
        return response.data
    } catch (error) {
        throw error
    }
}

// Generated by: typescript-json-schema --required --noExtraProps tsconfig.json 'Product_IdOptional_NoShopifyId'
export const Product_IdOptional_NoShopifyId = {
    "$schema": "http://json-schema.org/draft-07/schema#",
    "additionalProperties": false,
    "properties": {
        "_id": {
            "type": "string"
        },
        "initialOptions": {
            "additionalProperties": {
                "items": {
                    "type": "string"
                },
                "type": "array"
            },
            "type": "object"
        },
        "name": {
            "type": "string"
        },
        "printType": {
            "type": "string"
        },
        "productCategoryId": {
            "type": "string"
        },
        "productType": {
            "type": "string"
        }
    },
    "required": [
        "initialOptions",
        "name",
        "printType",
        "productCategoryId",
        "productType"
    ],
    "type": "object"
}

makeVariablesGlobal({ validate, Product_IdOptional_NoShopifyId })

export const updateProduct = async (id: string, product: Product_NoShopifyId): Promise<ProductType> => {
    try {
        const response = await axios.post(`${API_URL}/api/updateProduct`,
            { id: id, product: product })
        validate(response.data, { "type": "object" }, { throwError: true })
        return response.data
    } catch (error) {
        throw error
    }
}

export const updateOptionValue = async (id: string, newFields: Partial<OptionValue>): Promise<OptionValue> => {
    try {
        const response = await axios.post(`${API_URL}/api/updateOptionValue`,
            { id: id, ...newFields })
        validate(response.data, { "type": "object" }, { throwError: true })
        return response.data
    } catch (error) {
        throw error
    }
}

export const findCategoryWithId = (id: string,
    items: ProductCategory[]): ProductCategory | undefined => {
    return R.find(R.where({ _id: R.equals(id) }), items)
}

export const findProductWithId = (id: string,
    items: ProductType[]): ProductType | undefined => {
    return R.find(R.where({ _id: R.equals(id) }), items)
}



export const findOptionValueWithId = (optionValueId: string,
    optionValues: OptionValue[]) => {
    return R.find(R.where({ _id: R.equals(optionValueId) }), optionValues)
}

export const extractProductOptionWithId = (optionId: string,
    productOptions: ProductOption[]): ProductOption | undefined => {
    return R.find(R.where({ _id: R.equals(optionId) }), productOptions)
}

export const setProductOptionsInStore = async (currentProductOptions: ProductOption[],
    setProductOptions: (x: ProductOption[]) => void) => {
    if (R.isEmpty(currentProductOptions)) {
        console.log('product options is empty')

        const fetched = await fetchProductOptions()

        setProductOptions(fetched)
    }
}

export const setOptionValuesInStore = async (currentOptionValues: OptionValue[],
    setOptionValues: (x: OptionValue[]) => void) => {
    if (R.isEmpty(currentOptionValues)) {
        console.log('option values is empty')

        const fetched = await fetchOptionValues()

        setOptionValues(fetched)
    }
}

export const fetchProductCategories = async (): Promise<ProductCategory[]> => {
    try {
        const response = await axios.get(`${API_URL}/api/getProductCategories`)
        //console.log();        
        const t = validate(response.data, { "type": "array", "items": { "type": "object" } })
        makeVariablesGlobal({ t })
        return response.data
    } catch (error) {
        throw error
    }
}

/**
 * If currentProducts is empty, call the api and store the results in the redux store.
 * @param currentProducts 
 * @param setProducts 
 * @param setProductDescriptions 
 */
export const setProductsAndProductDescriptions = async (
    currentProducts: ProductType[],
    setProducts: (x: ProductType[]) => void,
    setProductDescriptions: (x: ProductDescriptionType[]) => void) => {
    if (R.isEmpty(currentProducts)) {
        console.log('products is empty');

        const getId = R.pluck('id')

        // Todo: await blocks the next await, unless you await the reference.
        const products = await getProducts()

        setProducts(products)

        const productIds: string[] = getId(products)

        const productDescriptions = await getProductDescriptionsAndId(productIds)

        setProductDescriptions(productDescriptions)
    }
}

export const setProductCategoriesInStore = async (currentProductCategories: ProductCategory[],
    setProductCategories: (x: ProductCategory[]) => void) => {
    if (R.isEmpty(currentProductCategories)) {
        console.log('product categories is empty')

        const categories = await fetchProductCategories()

        setProductCategories(categories)
    }
}

if (process.browser) {
    (window as any).p_getProducts = getProducts;
    (window as any).p_getProductDescription = getProductDescription
}