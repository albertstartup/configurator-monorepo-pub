import { Menu } from 'antd'
import '../../styles/styles.css'
import Link from 'next/link'

const Header = () => {
    return <Menu className='navbar' mode='horizontal'>
        <Menu.Item>
            <Link href={`/`}>
                <a>Indie Printing Dashboard</a>
            </Link>
        </Menu.Item>
        <Menu.Item>
            <Link href={`/`}>
                <a>Home</a>
            </Link>
        </Menu.Item>
        <Menu.Item>
            <Link href={`/CreateCategory`}>
                <a>Create A Category</a>
            </Link>
        </Menu.Item>
        <Menu.Item>
            <Link href={`/chooseProduct`}>
                <a>Edit A Product</a>
            </Link>
        </Menu.Item>
        <Menu.Item>
            <Link href={`/createProduct`}>
                <a>Create A Product</a>
            </Link>
        </Menu.Item>
    </Menu>
}

export default Header