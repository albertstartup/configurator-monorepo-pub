import { equals, always, where, pluck, filter, includes } from 'ramda'
import { ProductType, ProductDescriptionType } from '../../..';

export type ProductSelectorFilterObj = {
    productCategoryId: string,
    printType: string,
    productType: string
}

const filterProductsWithDescriptions = (products: ProductType[], filterObj: ProductSelectorFilterObj) => {
    const spec = {
        productCategoryId: filterObj.productCategoryId ? equals(filterObj.productCategoryId) : always(true),
        printType: filterObj.printType ? equals(filterObj.printType) : always(true),
        productType: filterObj.productType ? equals(filterObj.productType) : always(true)
    }    
    const filteredProducts: ProductType[] = filter(where(spec), products)
    // const filteredIds = pluck("productId", filteredproductDescriptions)
    // const filteredProducts = filter((product) => includes(product._id, filteredIds),
    //     products)
    return filteredProducts
}

export default filterProductsWithDescriptions