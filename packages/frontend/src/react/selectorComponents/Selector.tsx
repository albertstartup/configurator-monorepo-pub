import * as R from 'ramda'
import Button from 'antd/lib/button'
import { FunctionComponent, useState, JSXElementConstructor, useEffect } from 'react';

export type Item = Partial<Record<'name' | '_id', string>>

type Props<T> = {
    items: T[];
    onSelect?: (newSelectedItems: T[]) => void,
    multiple?: boolean,
    defaultSelectedItems?: T[],
    disabledItems?: T[],
    style?: object
}

const isItemSelected = <T extends {}>(item: T, selectedItems: T[]) => {
    return R.includes(item, selectedItems)
}

const isItemDisabled = <T extends {}>(item: T, disabledItems: T[]) => {
    return R.includes(item, disabledItems)
}

const newSelectedItems:
    <T extends {}>(n: T, s: T[], m: boolean) => T[]
    = (newSelection, selectedItems, mutliple) => {

        if (isItemSelected(newSelection, selectedItems)) {
            return R.without([newSelection], selectedItems)
        } else {
            return mutliple ? R.union([newSelection], selectedItems) : [newSelection]
        }
    }

const Selector: <T extends Item>(p: Props<T>) => React.ReactElement<Props<T>> = (props) => {
    const [selectedItems, setSelectedItems] =
        useState<typeof props.items>([])

    const defaultSelectedItems = props.defaultSelectedItems ? props.defaultSelectedItems : []
    
    useEffect(() => {
        setSelectedItems(defaultSelectedItems)      
    }, [defaultSelectedItems.length])

    return <div style={props.style}>
        {
            R.map((item) => {
                return <Button onClick={() => {
                    const onSelect = props.onSelect
                    const newSelected = newSelectedItems(item, selectedItems, props.multiple || false)
                    setSelectedItems(newSelected)
                    onSelect && onSelect(newSelected)

                }} value={item.name}
                    type={isItemSelected(item, selectedItems) ? 'primary' : "default"}
                    style={{ marginLeft: '1em' }}
                    key={item._id}
                    disabled={isItemDisabled(item, props.disabledItems || [])}>{item.name}</Button>
            }, props.items)
        }
    </div>
}

export default Selector