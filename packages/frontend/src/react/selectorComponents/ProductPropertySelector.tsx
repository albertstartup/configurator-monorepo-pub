import * as R from 'ramda'
import { Button } from 'antd'
import { FunctionComponent, useState } from 'react';

type ProductPropertySelectorProps = {
    productProperties: string[],
    onSelect?: (value: string) => void
}

const ProductPropertySelector: FunctionComponent<ProductPropertySelectorProps> = (props) => {
    const { productProperties, onSelect } = props

    const [selectedValue, setSelectedValue] = useState('')

    return <div>
        {
            R.map((productProperty) => {
                return <Button onClick={(e: any) => {
                    if (!onSelect) return                    

                    const value = e.target.value                    

                    if (selectedValue === value) {
                        setSelectedValue('')                        
                        onSelect('')
                    } else {
                        setSelectedValue(value)
                        onSelect(value)
                    }                       
                }} value={productProperty}
                    type={selectedValue === productProperty ? "primary" : "dashed"}
                    style={{ marginLeft: '1em' }}
                    key={productProperty}>{productProperty}</Button>
            }, productProperties)
        }
    </div>
}

export default ProductPropertySelector