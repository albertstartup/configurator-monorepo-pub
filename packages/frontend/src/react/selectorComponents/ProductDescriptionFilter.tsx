import { FunctionComponent } from 'react';
import ProductPropertySelector from './ProductPropertySelector';
import { ProductType, ProductCategory } from '../../..';
import * as R from 'ramda'
import { CustomDivider } from '../lib/componentLib';
import Selector from './Selector';

type ProductDescriptionFilterProps = {
    productTypeDividerText: string,
    printTypeDividerText: string,
    productCategoryDivideText?: string,
    onSelectProductType?: (productType: string) => void,
    onSelectPrintType?: (x: string) => void,
    onSelectProductCategory?: (x: ProductCategory[]) => void,
    productCategories?: ProductCategory[],
    productTypes?: string[],
    printTypes?: string[]
}

const ProductDescriptionFilter: FunctionComponent<ProductDescriptionFilterProps> = (props) => {
    // This may be an anti pattern, it may be better to just use props.
    const { onSelectProductType, onSelectPrintType, onSelectProductCategory,
        productTypeDividerText, printTypeDividerText, productCategoryDivideText } = props
        
    return <>
        <CustomDivider text={productTypeDividerText} />

        <div style={{ marginBottom: '3em' }}>
            <ProductPropertySelector onSelect={onSelectProductType}
            productProperties={['Flat', 'Book', 'Large Format']} />
        </div>

        <CustomDivider text={printTypeDividerText} />

        <div style={{ marginBottom: '3em' }}>
            <ProductPropertySelector onSelect={onSelectPrintType}
            productProperties={['Digital', 'Offset']} />
        </div>

        {productCategoryDivideText && props.productCategories &&
            <>
                <CustomDivider text={productCategoryDivideText} />

                <div style={{ marginBottom: '3em' }}>
                    <Selector items={props.productCategories} onSelect={onSelectProductCategory}/>
                    {/* <ProductPropertySelector onSelect={onSelectProductCategory}
                    productProperties={R.pluck('name', props.productCategories)} /> */}
                </div>
            </>
        }
    </>
}

export default ProductDescriptionFilter