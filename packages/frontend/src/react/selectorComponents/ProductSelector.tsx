import { FunctionComponent } from "react";
import { CustomDivider } from '../lib/componentLib'
import { map } from "ramda";
import { Button } from "antd";
import filterProductsWithDescriptions, { ProductSelectorFilterObj } from "./filterProductsWithDescriptions"
import { ProductType, ProductDescriptionType } from "../../..";
import Link from "next/link";

type ProductSelectorProps = {
    dividerText: string,
    filterObj: ProductSelectorFilterObj,
    products: ProductType[],
    productDescriptions: ProductDescriptionType[],
}

const ProductSelector: FunctionComponent<ProductSelectorProps> = ({ dividerText, filterObj, products, productDescriptions }) => {

    const filteredProducts = filterProductsWithDescriptions(products, filterObj)

    return <>
        <CustomDivider text={dividerText} />
        {
            map((product) => {
                return <Link href={`/manageProduct?id=${product._id}`}>
                    <Button 
                    style={{ marginRight: '1em' }}
                    type='dashed'>{product.name}</Button>
                    </Link>
                    }, filteredProducts)
                }
    </>
            }

export default ProductSelector