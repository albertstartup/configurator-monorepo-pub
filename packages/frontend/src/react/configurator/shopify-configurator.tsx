import { render } from 'react-dom'
import Configurator from './ConfiguratorUI'

if (document) {            
    const target = document.getElementById('configuratorContainer')
    target && render(<Configurator
        //@ts-ignore
        initialOptions={window.private_initialOptions}
        //@ts-ignore 
        optionValues={window.private_optionValues}
        //@ts-ignore
        productOptions={window.private_productOptions}
    />, target)
}