import * as R from 'ramda'
import { OptionValueName, Invalidates, Selections, ProductOptions, OptionValue, OptionId, OptionValueId } from '../../..';

/**
 * Given an option value id and a list of option values,
 * find the option value and return the invalidates
 * @param optionValueId 
 * @param optionValues 
 */
const getInvalidatesOfOptionValueId = (optionValueId: string,
    optionValues: OptionValue[]): Invalidates => {
        const optionValue = R.find(optionValue => {
            return optionValue._id === optionValueId
        }, optionValues)
        if (!optionValue) throw Error('No option value with that id')
        return optionValue.invalidates
}

/**
 * A normal merge causes the array of option value names to be override by
 * the last object. Instead we want to concat the arrays.
 * @param allInvalidates 
 */
const mergeInvalidates = (allInvalidates: Invalidates[]): Invalidates => {
    return R.reduce((acc: Invalidates, elem) => {
        // Merge with goes through every key and then we concat the arrays.
        return R.mergeWith((valueA, valueB) => {
            return R.concat(valueA, valueB)
        }, acc, elem)
    }, {}, allInvalidates)
}

export const mergeSelectionInvalidates = (selections: Selections, optionValues: OptionValue[]): Invalidates => {
    const allSelectionsInvalidates = R.mapObjIndexed((selectedOptionValueIds: OptionValueId[]) => {
        // Map over the list of selected option values and return their Invalidates.
        const allInvalidates = R.map((selectedOptionValueId) => {
            return getInvalidatesOfOptionValueId(selectedOptionValueId, optionValues)
        }, selectedOptionValueIds)

        // Merge each selected option value's Invalidates.
        const mergedInvalidates: Invalidates = mergeInvalidates(allInvalidates)
        // R.mapAccum((accum, invalidates: Invalidates) => {
        //     return R.mergeDeepLeft(accum, invalidates)
        // }, allInvalidates)
        return mergedInvalidates
    }, selections)

    // Merge the invalidates of each selection.
    const merged: Invalidates = mergeInvalidates(R.values(allSelectionsInvalidates))

    return merged
}

export const getInvalidatedOptionValuesFromSelectionForOptionId = (optionId: string, selections: Selections,
    optionValues: OptionValue[]): OptionValue[] => {
        const invalidates = mergeSelectionInvalidates(selections, optionValues)
        const invalidatesForOptionId = invalidates[optionId]
        return R.filter<OptionValue>((optionValue) => R.includes(optionValue._id, invalidatesForOptionId), optionValues)
}

export const getNewProductOptionsFromSelections = (selections: Selections,
    currentProductOptions: ProductOptions,
    optionValues: OptionValue[]): ProductOptions => {
    // Merge the invalidates of each selection.
    const invalidates: Invalidates = mergeSelectionInvalidates(selections, optionValues)

    // Return the current product options, without the merged invalidates.
    const newProductOptions: ProductOptions = R.mapObjIndexed((optionValues: OptionValueName[],
        optionId: OptionId) => {

        const invalidatedOptionValues: OptionValueName[] = invalidates[optionId]
        return R.without(invalidatedOptionValues, optionValues)
    }, currentProductOptions)
    return newProductOptions
}

/**
 * Given an optionValueName belonging to an option, get the invalidate rules define
 * in the options object.
 * @param optionName 
 * @param optionValueName 
 * @param options 
 */
// const getInvalidatesOfOptionValue = (optionName: OptionName,
//     optionValueName: OptionValueName, globalOptions: GlobalOptions): Invalidates => {

//     const option = globalOptions[optionName]

//     if (!option) {
//         console.error(`Message From Programmer: globalOptions does not have option: ${optionName}`)
//         return {}
//     }

//     if (!option[optionValueName]) {
//         console.warn(`Message From Programmer:
//         ${optionName} does not have value ${optionValueName}`)
//         return {}
//     }

//     //const invalidates = option[optionValueName].invalidates
//     const invalidates = option[optionValueName].invalidates

//     if (!invalidates) {
//         console.error('Option value does not have invalidates')
//         return {}
//     }

//     return invalidates
// }