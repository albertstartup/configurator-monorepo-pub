import React from 'react'
import { FunctionComponent, useState } from "react";
import { render } from 'react-dom'
import { OptionValue, ProductOptions, ProductOption } from "../../..";
import { values, mapObjIndexed } from 'ramda'
import { getOptionValueWithIds, getOptionNameWithId } from "../lib/lib";
import Selector from "../selectorComponents/Selector";
import 'antd/dist/antd.css'
import { getNewProductOptionsFromSelections } from './configurator'

type Props = {
    optionValues: OptionValue[],
    initialOptions: ProductOptions,
    productOptions: ProductOption[]
}

const Configurator: FunctionComponent<Props> = (props) => {
    const [filteredOptionValues, setFilteredOptionValues] = useState(props.optionValues)

    return <div>
        {values(mapObjIndexed((optionValueIds, optionId) => {            
            const optionValues = getOptionValueWithIds(
                optionValueIds, props.optionValues)

            const optionName = getOptionNameWithId(optionId, props.productOptions)

            const multiple = optionName === 'Bindery' ? true : false

            return <div style={{ marginBottom: '2em' }}>
                <div style={{ marginBottom: '1em' }}>
                    Select {optionName}:
                </div>
                <Selector onSelect={() => {}} multiple={multiple} items={optionValues} />
            </div>
        }, props.initialOptions))}
    </div>
}

export default Configurator