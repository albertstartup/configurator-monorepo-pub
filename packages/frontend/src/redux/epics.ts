import { Epic } from 'redux-observable'
import { RootAction, RootState } from './reducers';
import { filter, switchMap, map, catchError, delay } from 'rxjs/operators'
import { from, of, pipe, Observable, defer } from 'rxjs';
import { isActionOf, createAsyncAction } from 'typesafe-actions';
import {
    fetchProductOptions, fetchProducts,
    fetchOptionValues, fetchProductCategories
} from '../react/lib/lib'
import { async_actions } from './actions'

export const fetchProductOptionsEpic: Epic<RootAction, RootAction> = (action$) =>
    action$.pipe(
        filter(isActionOf(async_actions.fetchProductOptions.request)),
        switchMap(action =>
            from(fetchProductOptions()).pipe(
                map(async_actions.fetchProductOptions.success),
                catchError(pipe((error) => { throw Error(error) }))
            )
        )
    )

export const fetchProductsEpic: Epic<RootAction, RootAction> = (action$) =>
    action$.pipe(
        filter(isActionOf(async_actions.fetchProducts.request)),
        switchMap(action =>
            from(fetchProducts()).pipe(
                map(async_actions.fetchProducts.success),
                catchError(pipe((error) => { throw Error(error) }))
            )
        )
    )

export const fetchOptionValuesEpic: Epic<RootAction, RootAction> = (action$) =>
    action$.pipe(
        filter(isActionOf(async_actions.fetchOptionValues.request)),
        switchMap(action =>
            from(fetchOptionValues()).pipe(
                map(async_actions.fetchOptionValues.success),
                catchError(pipe((error) => { throw Error(error) }))
            )
        )
    )

export const fetchProductCategoriesEpic: Epic<RootAction, RootAction> = (action$) =>
    action$.pipe(
        filter(isActionOf(async_actions.fetchProductCategories.request)),
        switchMap(action =>
            from(fetchProductCategories()).pipe(
                map(async_actions.fetchProductCategories.success),
                catchError(pipe((error) => { throw Error(error) }))
            )
        )
    )