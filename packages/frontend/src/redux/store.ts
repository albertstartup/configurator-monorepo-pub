import { compose, createStore, applyMiddleware } from 'redux'
import AppReducer, { RootAction, RootState } from './reducers'
import { makeVariablesGlobal } from '../react/lib/lib';
import { composeWithDevTools } from 'redux-devtools-extension'
import { createEpicMiddleware, combineEpics } from 'redux-observable'
import * as epics from './epics'

const epicMiddleware = createEpicMiddleware()

const store = createStore(AppReducer, composeWithDevTools(
    applyMiddleware(epicMiddleware)
))

const combined = combineEpics(...Object.values(epics))

//@ts-ignore Redux's Action is not equal to typesafe-action's PayloadAction.
epicMiddleware.run(combined)

makeVariablesGlobal({ store })

export default store