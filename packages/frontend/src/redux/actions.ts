import { createAction, createAsyncAction } from 'typesafe-actions';
import { ProductType, ProductDescriptionType, ProductCategory, ProductOption, OptionValue } from '../..';

const fetchProductOptions = createAsyncAction(
    'FETCH_PRODUCT_OPTIONS_REQUEST',
    'FETCH_PRODUCT_OPTIONS_SUCCESS',
    'FETCH_PRODUCT_OPTIONS_FAILURE'
)<null, ProductOption[], Error>()

const fetchProducts = createAsyncAction(
    'FETCH_PRODUCT_REQUEST',
    'FETCH_PRODUCT_SUCCESS',
    'FETCH_PRODUCT_FAILURE'
)<null, ProductType[], Error>()

const fetchProductCategories = createAsyncAction(
    'FETCH_PRODUCT_CATEGORIES_REQUEST',
    'FETCH_PRODUCT_CATEGORIES_SUCCESS',
    'FETCH_PRODUCT_CATEGORIES_FAILURE'
)<null, ProductCategory[], Error>()

const fetchOptionValues = createAsyncAction(
    'FETCH_OPTION_VALUES_REQUEST',
    'FETCH_OPTION_VALUES_SUCCESS',
    'FETCH_OPTION_VALUES_FAILURE'
)<null, OptionValue[], Error>()

export const async_actions = { fetchProducts, fetchProductCategories,
    fetchProductOptions, fetchOptionValues }

export const setProducts = createAction('products/SET', (resolve) => {
    return (products: ProductType[]) => resolve(products)
})

export const setProductDescriptions = createAction('productDescriptions/SET', (resolve) => {
    return (x: ProductDescriptionType[]) => resolve(x)
})

export const setProductCategories = createAction('productCategories/SET', (resolve) => {
    return (x: ProductCategory[]) => resolve(x)
})

export const setProductOptions = createAction('productOptions/SET', (resolve) => {
    return (x: ProductOption[]) => resolve(x)
})

export const setOptionValues = createAction('optionValues/SET', (resolve) => {
    return (x: OptionValue[]) => resolve(x)
})

export const activateSpinner = createAction('spinner/ACTIVATE', (resolve) => {
    return () => resolve()
})

export const deactivateSpinner = createAction('spinner/DEACTIVATE', (resolve) => {
    return () => resolve()
})