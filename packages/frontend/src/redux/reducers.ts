import { ActionType, getType, StateType } from 'typesafe-actions'

import * as actions from './actions'
import { ProductType, ProductDescriptionType, ProductCategory, ProductOption, OptionValue } from '../..';

export type AppActions = ActionType<typeof actions>

export type RootAction = AppActions
export type RootState = StateType<typeof rootReducer>

type State = {
    products: ProductType[],
    productDescriptions: ProductDescriptionType[],
    productCategories: ProductCategory[],
    productOptions: ProductOption[],
    optionValues: OptionValue[],
    spinnerActive: boolean,
}

const initialState: State = {
    products: [],
    productDescriptions: [],
    productCategories: [],
    productOptions: [],
    optionValues: [],
    spinnerActive: false
}

const rootReducer = (state = initialState, action: RootAction) => {
    switch (action.type) {
        case getType(actions.setProducts):
            return { ...state, products: action.payload }
        case getType(actions.setProductDescriptions):
            return { ...state, productDescriptions: action.payload }
        case getType(actions.setProductCategories):
            return { ...state, productCategories: action.payload }
        case getType(actions.setProductOptions):
            return { ...state, productOptions: action.payload }
        case getType(actions.setOptionValues):
            return { ...state, optionValues: action.payload }
        case getType(actions.activateSpinner):
            return { ...state, spinnerActive: true }
        case getType(actions.deactivateSpinner):
            return { ...state, spinnerActive: false }
        // Async Actions
        case getType(actions.async_actions.fetchProductOptions.request):
            return { ...state, spinnerActive: true }
        case getType(actions.async_actions.fetchProductOptions.success):
            return { ...state, spinnerActive: false, productOptions: action.payload }
        case getType(actions.async_actions.fetchProductOptions.failure):
            return { ...state }
        //
        case getType(actions.async_actions.fetchProducts.request):
            return { ...state, spinnerActive: true }
        case getType(actions.async_actions.fetchProducts.success):
            return { ...state, spinnerActive: false, products: action.payload }
        case getType(actions.async_actions.fetchProducts.failure):
            return { ...state }
        //
        case getType(actions.async_actions.fetchProductCategories.request):
            return { ...state, spinnerActive: true }
        case getType(actions.async_actions.fetchProductCategories.success):
            return { ...state, spinnerActive: false, productCategories: action.payload }
        case getType(actions.async_actions.fetchProductCategories.failure):
            return { ...state }
        //
        case getType(actions.async_actions.fetchOptionValues.request):
            return { ...state, spinnerActive: true }
        case getType(actions.async_actions.fetchOptionValues.success):
            return { ...state, spinnerActive: false, optionValues: action.payload }
        case getType(actions.async_actions.fetchOptionValues.failure):
            return { ...state }
        default:
            return state
    }
}

export default rootReducer