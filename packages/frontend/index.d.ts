export declare type API_URL = string

type Overwrite<T1, T2> = {
    [P in Exclude<keyof T1, keyof T2>]: T1[P]
} & T2

type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>

type MongoIdOptional<T> = Overwrite<T, {
    _id?: string
}>

export type ShopifyProductId = string
export type ProductType = {
  _id: string,
  name: string,
  initialOptions: ProductOptions,
  printType: string,
  productType: string,
  productCategoryId: string,
  shopifyProductId: ShopifyProductId
}

type Product_IdOptional = MongoIdOptional<ProductType>

type Product_IdOptional_NoShopifyId = Omit<Product_IdOptional, "shopifyProductId">

type Product_NoId_NoShopifyId = Omit<Omit<Product_IdOptional, "shopifyProductId">, "_id">

type Product_NoShopifyId = Omit<ProductType, "shopifyProductId">

export type ProductCategory = {
    _id: string,
    name: string
}

export type ProductDescriptionType = {
    productId: string,
    productType: string,
    printType: string
}

export type OptionValueName = string
export type OptionName = string
export type OptionId = string
export type OptionValueId = string

type InvalidatedOptionValueName = string

export type Selections = {
    [optionNameId: string]: OptionValueId[]
}

export type Invalidates = {
    [productOptionId: string]: string[]
}

export type OptionValues = {
    [optionValueName: string]: {
        id: OptionValueName,
        invalidates: Invalidates
    }
}

export type OptionValue = {
    _id: string,
    name: string,
    ownerOptionId: string
    invalidates: Invalidates
}

export type ProductOption = {
    _id: string,
    name: string,
    //optionValueIds: string[]
}

export type GlobalOptions = {
    [optionName: string]: OptionValues
}

export type ProductOptions = {
    [optionNameId: string]: OptionValueId[]
}