import { getNewProductOptionsFromSelections } from '../src/react/configurator/configurator';
import { Selections, ProductOptions, OptionValue } from '..';

import { filterOptionValuesByOwnerOptionIds } from '../src/react/lib/lib';
//const jsc = require('jsverify') 

test('filterOptionValuesByOwnerOptionIds', () => {

    const optionValue1: OptionValue = {
        _id: '1',
        name: '1',
        ownerOptionId: '3',
        invalidates: {}
    }
    const optionValue2: OptionValue = {
        _id: '1',
        name: '1',
        ownerOptionId: '2',
        invalidates: {}
    }

    const ownerOptionIds = ['1', '2']
    expect(filterOptionValuesByOwnerOptionIds(ownerOptionIds, [optionValue1, optionValue2]))
        .toEqual([optionValue2])
})

test('configurator', () => {
    const optionValues: OptionValue[] = [
        {
            _id: 'red', name: '', ownerOptionId: '', invalidates: {
                sizes: ['small']
            }
        },
        {
            _id: 'blue', name: '', ownerOptionId: '', invalidates: {
                sizes: ['medium']
            }
        },
        {
            _id: '250', name: '', ownerOptionId: '', invalidates: {
                sizes: ['xlarge']
            }
        },
    ]

    const selections: Selections = {
        colors: ['red', 'blue'],
        quantities: ['250']
    }
    const productOptions: ProductOptions = {
        sizes: ['small', 'medium', 'large', 'xlarge'],
    }


    expect(getNewProductOptionsFromSelections(selections,
        productOptions, optionValues)).toEqual({
            sizes: ['large']
        })
})

// JSVerify test for configurator.
// test('configurator', () => {

//     const arbInvalidates = jsc.record({ 'color': jsc.array(jsc.string), 'size': jsc.array(jsc.string) })

//     const result = jsc.assertForall(jsc.string, jsc.string, arbInvalidates, (selectedOptionValue: OptionValueName,
//         selectedOption: OptionName, invalidates: Invalidates) => {
//         const invalidatesWithReason = createInvalidatesWithReasonFromSelectedOptionValue(selectedOptionValue,
//             selectedOption, invalidates)
//         const invalidateWithReason = invalidatesWithReason[selectedOptionValue][0]
//         return invalidateWithReason.reason === selectedOption + ',' + selectedOptionValue
//     })

//     return Promise.resolve(result)
// })