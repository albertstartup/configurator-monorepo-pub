import {Express, Request, Response, NextFunction} from 'express'
import Shopify, { IProduct } from 'shopify-api-node'
import { getProductDescriptionMetafield } from './lib';

type ProductDescriptionType = {
    productId: string,
    productType: string,
    printType: string
}

module.exports = function(app: Express, shopify: Shopify) {
    app.get('/api/getProductDescription/:productId', (req, res, next) => {
        const productId = req.params['productId'];
    
        shopify.metafield.list({
            metafield: { owner_resource: 'product', owner_id: productId }
        }).then((mfs) => {
            const productDescriptionMetafield = getProductDescriptionMetafield(mfs);
            if (productDescriptionMetafield) {
                console.log('productDescription: ', productDescriptionMetafield);
                res.send(productDescriptionMetafield.value);
            } else {
                res.send({ error: 'no product description metafield' });
            }
        }).catch(err => {
            res.send({ error: err })
            console.log(err);
        });
    });
    
    const addProductDescription = async (productId: string, description: ProductDescriptionType) => {
        return shopify.metafield.create({
            key: 'productDescription',
            value: JSON.stringify(description),
            value_type: 'json_string',
            namespace: 'inpr',
            owner_resource: 'product',
            owner_id: productId
        }).then(mf => {
            //@ts-ignore
            res.send(mf);
            console.log('added product description', mf);
        }).catch((err) => {
            //@ts-ignore
            res.send(err);
            console.log('error adding product description');
        });
    };
    
    app.post('/addProductDescription/:productId', async (req: Request, res: Response, next: NextFunction) => {
        const productId = req.params['productId'];
        const description = req.body
    
        if (req.body && req.body.productType && req.body.printType && req.body.productCategory) {
            addProductDescription(productId, description);
        } else {
            res.send({ error: 'Incorrect body to add product description' });
        }
    });
    
    // app.post('/updateProductDescription/:productId', async (req: Request, res: Response, next: NextFunction) => {
    //     const productId = req.params['productId'];
    //     const description = req.body;
    
    //     try {
    //         const results = await shopify.metafield.list({
    //             metafield: { owner_resource: 'product', owner_id: productId }
    //         });
    
    //         let productDescriptionMetafield = results && getProductDescriptionMetafield(results);
    
    //         if (!productDescriptionMetafield) {
    //             addProductDescription(productId, description).then(async (mf) => {
    //                 console.log('thing returned from add: ', mf);
    //                 res.send(mf);
    //             })
    //         }
    
    //         if (productDescriptionMetafield.id && req.body && req.body.productType && req.body.printType && req.body.productCategory) {
    //             shopify.metafield.update(productDescriptionMetafield.id, {
    //                 value: description,
    //                 value_type: 'json_string'
    //             }).then(mf => {
    //                 res.send(mf);
    //                 console.log('updated product description', mf);
    //             }).catch((err) => {
    //                 res.send(err);
    //                 console.log('error updating product description');
    //             });
    //         } else {
    //             res.send({ error: 'Incorrect body to add product description' });
    //         }
    
    //     } catch (err) {
    //         console.log(err);
    //         res.send({ error: err });
    //     }
    // });
}