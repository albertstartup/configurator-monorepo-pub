import { Express, Request, Response, NextFunction } from 'express'
import Shopify, { IProduct } from 'shopify-api-node'
import boom from 'boom'
import { asyncMiddleware, getMongoDatabase, createShopifyProduct, Product_IdOptional_NoShopifyId, Product, updateShopifyProduct } from './lib'
import { mongo_url } from './mongoSetup';
import { ObjectId, Db } from 'mongodb';
import { validate } from 'jsonschema'
import mongoose from 'mongoose'

import { prop, Typegoose, ModelType, InstanceType } from 'typegoose'

module.exports = function (app: Express, shopify: Shopify) {

    app.get('/api/products2', asyncMiddleware(async (req: Request, res: Response, next: NextFunction) => {
        try {
            const products: IProduct[] = await shopify.product.list()
            res.send(products)
        } catch (error) {
            throw boom.badRequest(error)
        }
    }))

    app.get('/api/getProducts', asyncMiddleware(async (req: Request, res: Response, next: NextFunction) => {
        try {
            const db = await getMongoDatabase(mongo_url)

            const cursor = await db.collection('products').find()

            const results: any[] = await cursor.map((doc) => {
                return doc
            }).toArray()

            res.send(results)
        } catch (error) {
            throw error
        }
    }))

    // app.post('/updateGlobalOptions', async (req: Request, res: Response, next: NextFunction) => {
    //     const newGlobalOptions = JSON.stringify(req.body);

    //     try {
    //         const metafields = await shopify.metafield.list({
    //             metafield: { owner_resource: 'shop' }
    //         });

    //         const globalOptionsMf = getGlobalOptionsMetafield(metafields);

    //         if (globalOptionsMf) {
    //             shopify.metafield.update(globalOptionsMf.id, {
    //                 id: globalOptionsMf.id,
    //                 value: newGlobalOptions,
    //                 value_type: 'json_string'
    //             }).then((mf) => {
    //                 //debugger;
    //                 console.log('updated');
    //                 res.send(mf);
    //             }).catch((err) => {
    //                 res.send(err);
    //             });
    //         }
    //     } catch (err) {
    //         res.send({ error: err })
    //     }
    // })

    // app.post('/api/createProduct', (req: Request, res: Response, next: NextFunction) => {
    //     if (req.body && req.body.productName &&
    //         req.body.configuratorDefaultState && req.body.productDescription) {
    //         //debugger;
    //         shopify.product.create({
    //             "title": req.body.productName,
    //             "metafields": [
    //                 {
    //                     key: 'configuratorDefaultState',
    //                     value: req.body.configuratorDefaultState,
    //                     value_type: 'json_string',
    //                     namespace: 'inpr'
    //                 },
    //                 {
    //                     key: 'productDescription',
    //                     value: req.body.productDescription,
    //                     value_type: 'json_string',
    //                     namespace: 'inpr'
    //                 }
    //             ]
    //         }).then((response) => {
    //             res.send(response)
    //         }).catch((err) => {
    //             res.send({ error: err })
    //         });
    //     } else {
    //         res.send({ error: 'Missing json items' });
    //     }
    // })

    app.post('/api/createProduct', asyncMiddleware(async (req: Request, res: Response, next: NextFunction) => {        
        try {
            const { product: maybeProduct } = req.body as { product: Product }        

            if (!validate(maybeProduct, Product_IdOptional_NoShopifyId).valid) throw boom.badRequest('JSON supplied did not match schema.')
            // Product variable may have _id field.
            const {_id: requestProductId, ...product} = maybeProduct

            const db = await getMongoDatabase(mongo_url)

            // Create in Mongo DB first to prevent duplicates.
            const createResponse = await db.collection('products').insertOne(product)
            if (createResponse.result.ok !== 1) throw boom.badRequest('Could not create in mongo.')

            const shopifyProductId = await createShopifyProduct(product.name,
                product.initialOptions, shopify, db)

            const updateResponse = await db
                .collection('products').findOneAndUpdate(
                    { _id: new ObjectId(createResponse.ops[0]._id) },
                    { $set: { shopifyProductId: shopifyProductId } },
                    { returnOriginal: false })

            if (updateResponse.ok === 1) {
                console.log('Created Product!')
                res.send(updateResponse.value)
            } else {
                throw boom.badRequest()
            }
        } catch (error) {
            throw error
        }
    }))    

    app.post('/api/updateProduct', asyncMiddleware(async (req: Request, res: Response, next: NextFunction) => {
        try {            
            const { product: maybeProduct } = req.body as { product: Product }
            
            if (!maybeProduct._id) throw Error('Request does not have _id.')
            if (typeof maybeProduct._id === 'string') maybeProduct._id = new ObjectId(maybeProduct._id)
            
            await mongoose.connect(mongo_url)

            const ProductModel = new Product().getModelForClass(Product)            

            const fetchedProduct = await ProductModel.findById(maybeProduct._id)

            if (!fetchedProduct) throw Error('Product to update could not be found.')

            fetchedProduct.set(maybeProduct)
            await fetchedProduct.save()

            if (!fetchedProduct.shopifyProductId) throw Error('The product does not have a Shopify Id.')

            await updateShopifyProduct(fetchedProduct.shopifyProductId,
                fetchedProduct.initialOptions, fetchedProduct.name, shopify)

            res.send(fetchedProduct)
            
            // if (!validate(maybeProduct, Product_IdOptional_NoShopifyId).valid) throw boom.badRequest('JSON supplied did not match schema.')

            // Product variable may have _id field.
            // const {_id: requestProductId, ...product} = maybeProduct

            // if (!requestProductId) { throw boom.badRequest('No id supplied.') }

            //const db = await getMongoDatabase(mongo_url)

            // const response = await db.collection('products')
            //     .findOneAndUpdate({ _id: new ObjectId(requestProductId) }, { $set: { ...product } }, {returnOriginal: false})                            

            // if (response.ok === 1) {
                //const doc = await db.collection('products').findOne({ "_id": new ObjectId(requestProductId) })
                // if (!response.value.shopifyProductId) throw Error('Updated object does not have shopifyProductId')
                // const updateProduct = response.value as Product

                //updateShopifyProduct(response.value.shopifyProductId, )
                // res.send(response.value)
            // } else {
            //     throw boom.badRequest()
            // }
        } catch (error) {
            throw error
        }
    }))

    app.get('/api/product/:productId', asyncMiddleware(async (req: Request, res: Response, next: NextFunction) => {
        const productId = req.params['productId'];

        try {
            const product = await shopify.product.get(productId);
            res.send(product);
        } catch (err) {
            //res.send({ error: err })
        }
    }))

    app.post('/api/updateProductName/:productId/:newName', async (req: Request, res: Response, next: NextFunction) => {
        const productId = req.params['productId'];
        const newName = req.params['newName']

        try {
            const response = await shopify.product
                .update(productId, {
                    title: newName
                })

            res.send(response)
        } catch (error) {
            next(error)
        }
    })

}