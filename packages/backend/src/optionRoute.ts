import { Express, Request, Response, NextFunction } from 'express'
import Shopify, { IProduct } from 'shopify-api-node'
import {
    getGlobalOptionsMetafield, asyncMiddleware,
    fetchGlobalOptions, getMongoDatabase, updateOrCreateShopifyMongoMetafield
} from './lib';
import boom from 'boom';
import { mongo_url } from './mongoSetup';
import { ObjectId, Db } from 'mongodb'

module.exports = function (app: Express, shopify: Shopify) {

    app.get('/api/getProductOptions', asyncMiddleware(async (req: Request, res: Response, next: NextFunction) => {
        try {
            const db = await getMongoDatabase(mongo_url)

            const cursor = await db.collection('productOptions').find()

            const results: any[] = await cursor.map((doc) => {
                return doc
            }).toArray()

            res.send(results)
        } catch (error) {
            throw error
        }
    }))

    app.post('/api/createProductOption/:optionName', asyncMiddleware(async (req: Request, res: Response, next: NextFunction) => {
        try {
            const optionName = req.params['optionName']

            const db = await getMongoDatabase(mongo_url)

            const response = await db.collection('productOptions').insertOne({ name: optionName })

            if (response.result.ok === 1) {
                await updateOrCreateShopifyMongoMetafield('productOptions', shopify, db)
                res.send(response.ops[0])
            } else {
                throw boom.badRequest()
            }
        } catch (error) {
            throw error
        }
    }))

    app.get('/api/getOptionValues', asyncMiddleware(async (req: Request, res: Response, next: NextFunction) => {
        try {
            const db = await getMongoDatabase(mongo_url);

            const cursor = await db.collection('optionValues').find()

            const results: any[] = await cursor.map((doc) => {
                return doc
            }).toArray()

            res.send(results)
        } catch (error) {
            throw error
        }
    }))

    app.post('/api/createOptionValue', asyncMiddleware(async (req: Request, res: Response, next: NextFunction) => {
        try {
            const name = req.body['name']
            const ownerOptionId = req.body['ownerOptionId']

            if (!name || !ownerOptionId) { throw boom.badRequest() }

            const db = await getMongoDatabase(mongo_url)

            const response = await db.collection('optionValues').insertOne({ name: name, ownerOptionId: ownerOptionId })

            if (response.result.ok === 1) {                
                updateOrCreateShopifyMongoMetafield('optionValues', shopify, db)
                res.send(response.ops[0])
            } else {
                throw boom.badRequest()
            }
        } catch (error) {
            throw error
        }
    }))

    const debugMe = async () => {
        try {
            const db = await getMongoDatabase(mongo_url);

            (global as any).g_db = db;
            (global as any).g_shopify = shopify

            const p = await shopify.product.get(3237407260736)
            const l = await shopify.metafield.list({
                metafield: {
                    owner_resource: "product",
                    owner_id: 3237407260736
                }
            })
            // const m = await shopify.metafield.update(7001411387456, {
            //     value: "{}",
            //     value_type: 'json_string'
            // })            

            console.log('debugMe start')
            console.log('debugMe end')
        } catch (error) {
            console.log(error)
        }
    }
    const makeNodeGlobal = (obj: {}) => {
        (global as any).p = { ...obj }
    }
    (global as any).debugMe = debugMe;
    (global as any).set = makeNodeGlobal

    app.post('/api/updateOptionValue', asyncMiddleware(async (req: Request, res: Response, next: NextFunction) => {
        try {
            const id = req.body['id']
            const invalidates = req.body['invalidates']

            if (!id || !invalidates) { throw boom.badRequest() }

            const db = await getMongoDatabase(mongo_url)

            const response = await db.collection('optionValues')
                .updateOne({ _id: new ObjectId(id) }, { $set: { invalidates: invalidates } })

            if (response.result.ok === 1) {
                const doc = await db.collection('optionValues').findOne({ "_id": new ObjectId(id) })
                updateOrCreateShopifyMongoMetafield('optionValues', shopify, db)
                res.send(doc)
            } else {
                throw boom.badRequest()
            }
        } catch (error) {
            throw error
        }
    }))

    app.post('/api/createProductOptionOld/:optionName', asyncMiddleware(async (req: Request, res: Response, next: NextFunction) => {
        try {
            const optionName: string = req.params.optionName

            if (!optionName) { throw boom.badRequest() }

            const db = await getMongoDatabase(mongo_url)

            const response = await db.collection('productOptions').insert({ name: optionName })

            if (response.result.ok === 1) {
                res.send({ createdId: response.ops[0]._id })
            } else {
                throw boom.badRequest()
            }
        } catch (error) {
            throw error
        }
    }))

    app.post('/updateGlobalOptions', async (req: Request, res: Response, next: NextFunction) => {
        const newGlobalOptions = JSON.stringify(req.body);

        try {
            const metafields = await shopify.metafield.list({
                metafield: { owner_resource: 'shop' }
            });

            const globalOptionsMf = getGlobalOptionsMetafield(metafields);

            if (globalOptionsMf) {
                shopify.metafield.update(globalOptionsMf.id, {
                    id: globalOptionsMf.id,
                    value: newGlobalOptions,
                    value_type: 'json_string'
                }).then((mf) => {
                    //debugger;
                    console.log('updated');
                    res.send(mf);
                }).catch((err) => {
                    res.send(err);
                });
            }
        } catch (err) {
            res.send({ error: err })
        }
    })

    app.get('/getGlobalOptions', async (req: Request, res: Response, next: NextFunction) => {

    });

    app.get('/getProductCategories', asyncMiddleware(async () => {
        const shopMetafields = await fetchGlobalOptions(shopify)
    }))

    // const fetchOptions = async () => {
    //     try {
    //         const shopMetafields = await shopify.metafield.list({
    //             metafield: { owner_resource: 'shop' }
    //         });

    //        const globalOptionsMf = getGlobalOptionsMetafield(metafields);

    //         if (globalOptionsMf) {
    //             res.send(globalOptionsMf.value)
    //         }
    //     } catch (err) {
    //         res.send({ error: err })
    //     }
    // }
}