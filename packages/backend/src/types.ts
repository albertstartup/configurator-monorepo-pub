export type OptionValueName = string

export type OptionName = string

export type Options = {
    [option: string]: Option
}

export type OptionValue = {
    invalidates: {}
}

export type Option = {
    [optionValue: string]: OptionValue
}