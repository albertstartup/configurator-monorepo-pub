import _ from 'underscore'
import Shopify, { IMetafield, IPrivateShopifyConfig, IPublicShopifyConfig } from 'shopify-api-node'
import { Express, Request, Response, NextFunction } from 'express'
import boom from 'boom'
import * as R from 'ramda'
import mongodb, { Db, ObjectId } from 'mongodb'
import { Typegoose, prop } from 'typegoose'

export type StoreMetafields = IMetafield[]

export type OptionValueName = string
export type OptionName = string

type InvalidatedOptionValueName = string

type OptionValueId = string

export type Invalidates = {
  [invalidatedOptionName: string]: InvalidatedOptionValueName[]
}

export class Product extends Typegoose {
  @prop()
  _id?: string | ObjectId;
  @prop({ required: true })
  name!: string;
  @prop({ required: true })
  initialOptions!: ProductOptions;
  @prop({ required: true })
  printType!: string;
  @prop({ required: true })
  productType!: string;
  @prop({ required: true })
  productCategoryId!: string;
  @prop()
  shopifyProductId?: ShopifyProductId
  @prop({ required: true })
  basePrice!: number
}

// export type Product = {
//   _id: string,
//   name: string,
//   initialOptions: ProductOptions,
//   printType: string,
//   productType: string,
//   productCategoryId: string,
//   shopifyProductId: ShopifyProductId
// }

export type OptionValues = {
  [optionValueName: string]: {
    id: OptionValueName,
    invalidates: Invalidates
  }
}

export type GlobalOptions = {
  [optionName: string]: OptionValues
}

export type ProductOptions = {
  [optionName: string]: OptionValueId[]
}

export type ShopifyProductId = string

export const getGlobalOptionsMetafield = (metafields: IMetafield[]): IMetafield | undefined => {
  return _.findWhere(metafields, { key: 'globalOptions' });
}

export const extractProductOptionsMetafield = (metafields: IMetafield[]): IMetafield | undefined => {
  return R.find(R.where({ key: R.equals('productOptions') }), metafields)
}

export const extractMetafieldWithName = (name: string, metafields: IMetafield[]): IMetafield | undefined => {
  return R.find(R.where({ key: R.equals(name) }), metafields)
}

export const extractOptionValueMetafield = (metafields: IMetafield[]): IMetafield | undefined => {
  return R.find(R.where({ key: R.equals('optionValues') }), metafields)
}

export const fetchShopMetafields = async (shopify: Shopify) => {
  try {
    const shopMetafields = await shopify.metafield.list({
      metafield: { owner_resource: 'shop' }
    })
    return shopMetafields
  } catch (error) {
    throw error
  }
}

export const getProductDescriptionMetafield = (metafields: IMetafield[]): IMetafield | undefined => {
  return _.findWhere(metafields, { key: 'productDescription' });
}

export const getDefaultStateMetafield = (metafields: IMetafield[]): IMetafield | undefined => {
  return _.findWhere(metafields, { key: 'configuratorDefaultState' });
};

export const extractProductOptions = (metafields: IMetafield[]): ProductOptions => {
  return extractJSONStringMetafieldValue(metafields, 'productOptions')
}

export const extractGlobalOptions = (storeMetafields: StoreMetafields): GlobalOptions => {
  return extractJSONStringMetafieldValue(storeMetafields, 'globalOptions')
}

export const extractJSONStringMetafieldValue = (metafields: IMetafield[], metafieldKey: string): Partial<{}> => {
  const metafield = R.find((metafield) => {
    return metafield.key === metafieldKey
  }, metafields)
  if (!metafield) throw Error('No metafield!')
  if (typeof metafield.value !== 'string') { throw Error('metafield value is not a string.') }
  return JSON.parse(metafield.value)
}

export const fetchProductMetafields = async (shopify: Shopify, productId: string): Promise<IMetafield[]> => {
  try {
    const metafields = await shopify.metafield.list({
      metafield: { owner_resource: 'product', owner_id: productId }
    })

    return metafields
  } catch (error) {
    throw error
  }
}

export const fetchProductOptions = async (shopify: Shopify, productId: string): Promise<ProductOptions> => {
  const product_metafields = await fetchProductMetafields(shopify, productId)
  return extractProductOptions(product_metafields)
}

export type JSON_STRING = string

// Todo: use typescript
export enum MetafieldOwnerResource {
  PRODUCT = 'product',
  SHOP = 'shop'
}

export enum MetafieldValueType {
  JSON_STRING = 'json_string',
  STRING = 'string'
}

export const createMetafield = async (shopify: any,
  key: string, value: {}, value_type: MetafieldValueType,
  owner_resource: MetafieldOwnerResource, ownerId?: string, namespace = "configuratorApp"): Promise<IMetafield> => {

  type CreateMetafieldParamsType = {
    key: string,
    value: {},
    owner_resource: string,
    ownerId?: string,
    namespace: string
  }

  const createMetafieldParams: CreateMetafieldParamsType = {
    key: key,
    value: value,
    owner_resource: owner_resource,
    namespace: namespace
  }
  if (owner_resource === MetafieldOwnerResource.PRODUCT && ownerId) {
    createMetafieldParams.ownerId = ownerId
  }

  try {
    const createdMetafield = await shopify.metafield.create(createMetafieldParams)
    return createdMetafield
  } catch (error) {
    throw error
  }
}

export const setProductCategoriesMetafield = async (shopify: Shopify, productCategories: string[]) => {
  const metafieldValue = JSON.stringify(productCategories)
  try {
    const createdMetafield = await shopify.metafield.create({
      key: 'productCategories',
      value: metafieldValue,
      value_type: 'json_string',
      namespace: 'inpr',
      owner_resource: 'shop'
    })
    return createMetafield
  } catch (error) {
    throw error
  }
}

export const fetchGlobalOptions = async (shopify: Shopify): Promise<GlobalOptions> => {
  try {
    const shopMetafields = await fetchShopMetafields(shopify)

    const globalOptions = extractGlobalOptions(shopMetafields)

    return globalOptions
  } catch (error) {
    throw error
  }
}

// This seems to make the try catch block for a route redundant.
export const asyncMiddleware = (fn: (req: Request, res: Response, next: NextFunction) => Promise<any>) => (req: Request, res: Response, next: NextFunction) => {
  Promise.resolve(fn(req, res, next)).catch((err) => {
    if (!err.isBoom) {
      return next(boom.badImplementation(err));
    }
    next(err);
  });
};

export const getMongoDatabase = async (mongo_url: string, ): Promise<mongodb.Db> => {
  console.log(mongo_url)

  const client = await mongodb.MongoClient.connect(mongo_url)

  const db = client.db()

  return db
}

export const errorHandler = (error: boom, req: Request, res: Response, next: NextFunction) => {
  if (error.isServer) {
    console.error('error :', error);
  }

  return res.status(error.output.statusCode).send(error.output.payload)
}

// Todo: Specifiy if shop or product metafield.
export const updateOrCreateShopifyMongoMetafield = async (collectionName: string,
  shopify: Shopify, db: Db) => {
  try {
    const metafields = await shopify.metafield.list({
      metafield: { owner_resource: 'shop' }
    })

    const metafield = extractMetafieldWithName(collectionName, metafields)

    const productOptions = await db.collection(collectionName).find().toArray()

    const value = JSON.stringify(productOptions)

    if (!metafield) {
      const createdMetafield = await shopify.metafield.create({
        key: collectionName,
        value: value,
        value_type: 'json_string',
        namespace: 'inpr',
        owner_resource: 'shop'
      })
      console.log('Created')
    } else {
      await shopify.metafield.update(metafield.id, {
        id: metafield.id,
        value: value,
        value_type: 'json_string'
      })
      console.log('Updated')
    }
  } catch (error) {
    throw error
  }
}

// Generated by: typescript-json-schema --required --noExtraProps tsconfig.json 'Product_IdOptional_NoShopifyId'
export const Product_IdOptional_NoShopifyId = {
  "$schema": "http://json-schema.org/draft-07/schema#",
  "additionalProperties": false,
  "properties": {
    "_id": {
      "type": "string"
    },
    "initialOptions": {
      "additionalProperties": {
        "items": {
          "type": "string"
        },
        "type": "array"
      },
      "type": "object"
    },
    "name": {
      "type": "string"
    },
    "printType": {
      "type": "string"
    },
    "productCategoryId": {
      "type": "string"
    },
    "productType": {
      "type": "string"
    }
  },
  "required": [
    "initialOptions",
    "name",
    "printType",
    "productCategoryId",
    "productType"
  ],
  "type": "object"
}

export const createShopifyProduct = async (name: string,
  initialOptions: ProductOptions,
  shopify: Shopify, db: Db): Promise<ShopifyProductId> => {
  try {
    const createdProduct = await shopify.product.create({
      "title": name,
      "metafields": [
        {
          key: 'initialOptions',
          value_type: 'json_string',
          namespace: 'inpr',
          value: JSON.stringify(initialOptions)
        }
      ]
    })
    console.log('Created Product!: ', createdProduct)
    return createdProduct.id.toString()
  } catch (error) {
    throw error
  }
}

export const updateShopifyProduct = async (shopifyProductId: string,
  initialOptions: ProductOptions, name: string,
  shopify: Shopify) => {
  // TODO: Update handle and seo title.
  try {
    const metafields = await shopify.metafield.list({
      metafield: {
        owner_resource: "product",
        owner_id: shopifyProductId
      }
    })
    const initialOptionsMetafield = extractMetafieldWithName('initialOptions', metafields)
    if (!initialOptionsMetafield) throw Error('initialOptions metafield does not exist.')

    const updatedMetafield = await shopify.metafield.update(initialOptionsMetafield.id, {
      value: JSON.stringify(initialOptions)
    })
    console.log('Updated initialOptions!: ', updatedMetafield)

    const updatedProduct = await shopify.product.update(parseInt(shopifyProductId), {
      id: shopifyProductId,
      title: name
    })

    console.log('Updated product!: ', updatedProduct)
  } catch (error) {
    throw error
  }
}