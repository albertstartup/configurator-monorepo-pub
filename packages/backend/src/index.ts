require('dotenv').config();

import express from 'express'
import Shopify from 'shopify-api-node'
import {errorHandler} from './lib'
const cors = require('cors');

const app = express();

app.use(cors());
app.use(express.json());

const port = process.env.PORT || 6942;

const env = process.env

if (!env.SHOPIFY_API_KEY || !env.SHOPIFY_API_PASSWORD || !env.SHOPIFY_SHOP_NAME) throw Error(
    'No Shopify API, Password, or Shop Name.')
const shopify = new Shopify({
    shopName: env.SHOPIFY_SHOP_NAME,
    apiKey: env.SHOPIFY_API_KEY,
    password: env.SHOPIFY_API_PASSWORD
})

require('./productRoutes')(app, shopify)
require('./defaultStateRoutes')(app, shopify)
require('./optionRoute')(app, shopify)
require('./productRoutes')(app, shopify)
require('./productDescriptionRoutes')(app, shopify)
require('./categoriesRoutes')(app, shopify)

app.use(errorHandler)
app.listen(port, () => console.log(`Listening on port: ${port}`))

// Creates globalOptions metafield
// shopify.metafield.create({
//     key: 'globalOptions',
//     value: JSON.stringify({}),
//     value_type: 'json_string',
//     namespace: 'inpr',
//     owner_resource: 'shop',
// }).then((response) => {
//     debugger
// });

// Business Card id: 2233299861568