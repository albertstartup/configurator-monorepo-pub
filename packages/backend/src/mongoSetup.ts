import { getMongoDatabase } from "./lib";

const environment = process.env.NODE_ENV || 'dev'

export let mongo_url: string

if (!process.env.MONGODB_URI) throw Error('No mongodb URI!')

if (environment === 'production') {
    mongo_url = process.env.MONGODB_URI
} else if (environment === 'dev') {
    mongo_url = 'mongodb://127.0.0.1:27017/configurator1'
}

const ensureIndexes = async () => {
    const db = await getMongoDatabase(mongo_url)

    db.collection('products').createIndex({name: 1}, {unique: true})
    db.collection('productOptions').createIndex({name: 1}, {unique: true})
    db.collection('optionValues').createIndex({name: 1}, {unique: true})
    db.collection('productCategories').createIndex({name: 1}, {unique: true})
}
ensureIndexes()

export default {}